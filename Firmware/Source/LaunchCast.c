/// @file
/// @brief Entry point and high level logic for the LaunchCast game.

#include <stdbool.h>
#include "TExaS.h"
#include "Controls.h"
#include "FrameTimer.h"
#include "Lights.h"
#include "Random.h"
#include "Screen.h"
#include "ScreenBuffer.h"
#include "Sound.h"
#include "Startup.h"
#include "Worlds.h"

#include "CalibrationScreen.h"
#include "GameScreen.h"
#include "InstructionsScreen.h"
#include "LogoScreen.h"
#include "LoseScreen.h"
#include "ScoreScreen.h"
#include "WinScreen.h"

/// @return Randomly selected #WorldData level geometry to play.
static const WorldData *_GetRandomWorldData(void) {
	// Drop least-significant bits of random result, as they repeat most frequently.
	static const unsigned char randomInsignificantBits = 8;
	return &worlds[(Random32() >> randomInsignificantBits) % worldCount];
}

/// @brief Entry point of the LaunchCast game.
int main(void) {
	// Shared buffer used to compose screen contents before writing to the display.
	static ScreenBuffer buffer;
	float timeRemaining;

	DisableInterrupts();
	Controls_Initialize();
	FrameTimer_Initialize();
	Lights_Initialize();
	Random_Init(0); // Start with a deterministic seed
	Screen_Initialize();
	Sound_Initialize();
	// Note: Must initialize TExaS last, since it uses new timer registers that
	// will cause a hard fault if set before their legacy registers are configured.
	TExaS_Init(SSI0_Real_Nokia5110_NoScope); // Enable PLL at 80 MHz
	EnableInterrupts();

	CalibrationScreen_Show(&buffer);
	while (true) {
		LogoScreen_Show(&buffer);
		InstructionsScreen_Show(&buffer);

		timeRemaining = GameScreen_Show(&buffer, _GetRandomWorldData());
		if (timeRemaining > 0.0f) { // Win
			WinScreen_Show(&buffer);
			ScoreScreen_Show(&buffer, timeRemaining);
		} else { // Lose
			LoseScreen_Show(&buffer);
		}
	}
}
