/// @file
/// @brief #ScreenBuffer type, along with drawing primitive routines for them.

#ifndef _SCREENBUFFER_H
#define _SCREENBUFFER_H

#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include "Bitmap.h"

/// @brief Width of a #ScreenBuffer, in pixels.
static const unsigned char screenBufferWidth = 84;
/// @brief Height of a #ScreenBuffer, in pixels.
static const unsigned char screenBufferHeight = 48;
/// @brief Byte length of a #ScreenBuffer's storage array.
static const size_t screenBufferLength = (size_t)screenBufferWidth * screenBufferHeight / CHAR_BIT;

/// @brief Pixel width of character glyphs.
static const unsigned char screenBufferGlyphWidth = 5;
/// @brief Pixel height of character glyphs.
static const unsigned char screenBufferGlyphHeight = 8;

/// @brief Buffer of 1-bit resolution pixel values to paint a Nokia 5110 screen with.
typedef struct ScreenBuffer {
	/// @brief Actual buffer of pixel bit values.
	/// Bytes are stored in row-major order originating at the upper left,
	/// while contained bits represent a column of pixels.
	unsigned char buffer[screenBufferLength];
} ScreenBuffer;

void ScreenBuffer_Clear(ScreenBuffer * const buffer);
void ScreenBuffer_Copy(ScreenBuffer * const destination, const ScreenBuffer * const origin);
void ScreenBuffer_PixelFill(ScreenBuffer * const buffer, const int x, const int y);
void ScreenBuffer_PixelClear(ScreenBuffer * const buffer, const int x, const int y);
void ScreenBuffer_RectangleSet(ScreenBuffer * const buffer, int left, int top, const unsigned char width, const unsigned char height, const bool value);
void ScreenBuffer_TextFill(ScreenBuffer * const buffer, int left, const int top, const char *text);
void ScreenBuffer_TextFillCentered(ScreenBuffer * const buffer, const int top, const char *text);
void ScreenBuffer_BitmapFill(ScreenBuffer * const buffer, int left, int top, const Bitmap * const bitmap);
void ScreenBuffer_BitmapFillColumn(ScreenBuffer * const buffer, int left, int top, const int bitmapColumn, const Bitmap * const bitmap);
void ScreenBuffer_BitmapFillCentered(ScreenBuffer * const buffer, const Bitmap * const bitmap);

#endif // !defined(_SCREENBUFFER_H)
