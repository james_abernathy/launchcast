/// @file
/// @brief A collection of #SoundSample structures to play.

#ifndef _SOUNDSAMPLES_H
#define _SOUNDSAMPLES_H

#include "Sound.h"

extern const SoundSample soundShoot;
extern const SoundSample soundExplosion;
extern const SoundSample soundExplosionBig;
extern const SoundSample soundInvader;
extern const SoundSample soundHighPitch;
extern const SoundSample soundClockTick;
extern const SoundSample soundClockTock;

#endif // !defined(_SOUNDSAMPLES_H)
