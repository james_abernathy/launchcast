/// @file
/// @brief Simple logo screen shown before new games.

#ifndef _LOGOSCREEN_H
#define _LOGOSCREEN_H

#include "ScreenBuffer.h"

void LogoScreen_Show(ScreenBuffer * const buffer);

#endif // !defined(_LOGOSCREEN_H)
