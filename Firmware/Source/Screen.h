/// @file
/// @brief High-level screen timing and repainting functions.

#ifndef _SCREEN_H
#define _SCREEN_H

#include "ScreenBuffer.h"

void Screen_Initialize(void);
void Screen_DrawBuffer(const ScreenBuffer *buffer);

#endif // !defined(_SCREEN_H)
