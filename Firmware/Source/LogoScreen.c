/// @file
/// @brief Simple logo screen shown before new games.

#include "LogoScreen.h"
#include "Controls.h"
#include "Screen.h"
#include "ScreenBuffer.h"
#include "Sound.h"
#include "SoundSamples.h"
#include "Sprites.h"

/// @brief Shows the game logo until the player presses a button.
/// @param[out] buffer Shared #ScreenBuffer to use for rendering.
void LogoScreen_Show(ScreenBuffer * const buffer) {
	Sound_PlaySample(&soundHighPitch, 1);
	ScreenBuffer_Clear(buffer);
	ScreenBuffer_BitmapFillCentered(buffer, &spriteLogo);
	Screen_DrawBuffer(buffer);
	Controls_WaitForButtonPress();
	Sound_PlaySample(&soundShoot, 1);
}
