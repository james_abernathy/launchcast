/// @file
/// @brief #WorldData structures for the game to choose from.

#ifndef _WORLDS_H
#define _WORLDS_H

#include <stddef.h>
#include "World.h"

extern const WorldData worlds[];
extern const size_t worldCount;

#endif // !defined(_WORLDS_H)
