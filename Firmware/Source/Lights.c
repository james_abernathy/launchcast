/// @file
/// @brief Routines for controlling the LEDs on the game box.

#include "Lights.h"
#include <stdbool.h>
#include "tm4c123gh6pm.h"

/// @brief Output pin mask for the top LED.
static const unsigned char _lightTopMask = 0x10; // PB4
/// @brief Output pin mask for the bottom LED.
static const unsigned char _lightBottomMask = 0x20; // PB5

/// @brief Initializes hardware to control game box LEDs.
void Lights_Initialize(void) {
	volatile unsigned long delay;
	static const unsigned char mask = _lightTopMask | _lightBottomMask;
	SYSCTL_RCGC2_R |= SYSCTL_RCGC2_GPIOB; // Enable clock
	delay = SYSCTL_RCGC2_R; // Wait for clock to stabilize
	GPIO_PORTB_AMSEL_R &= ~mask; // Disable analog function
	GPIO_PORTB_PCTL_R &= ~(GPIO_PCTL_PB4_M | GPIO_PCTL_PB4_M);
	GPIO_PORTB_DIR_R |= mask; // Outputs
	GPIO_PORTB_AFSEL_R &= ~mask; // No alternate function
	GPIO_PORTB_DEN_R |= mask; // Enable pins

	Lights_SetTop(false);
	Lights_SetBottom(false);
}

/// @return @c true if the top LED is on.
bool Lights_IsTopLit(void) {
	return (GPIO_PORTB_DATA_R & _lightTopMask) == _lightTopMask;
}

/// @return @c true if the bottom LED is on.
bool Lights_IsBottomLit(void) {
	return (GPIO_PORTB_DATA_R & _lightBottomMask) == _lightBottomMask;
}

/// @brief Sets the status of the top LED.
/// @param[in] enable @c true to light the LED.
void Lights_SetTop(const bool enable) {
	if (enable) {
		GPIO_PORTB_DATA_R |= _lightTopMask;
	} else {
		GPIO_PORTB_DATA_R &= ~_lightTopMask;
	}
}

/// @brief Sets the status of the bottom LED.
/// @param[in] enable @c true to light the LED.
void Lights_SetBottom(const bool enable) {
	if (enable) {
		GPIO_PORTB_DATA_R |= _lightBottomMask;
	} else {
		GPIO_PORTB_DATA_R &= ~_lightBottomMask;
	}
}
