/// @file
/// @brief #World game object and routines for manipulating it.

#include "World.h"
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include "Bitmap.h"
#include "ScreenBuffer.h"
#include "Sprites.h"

/// @brief Loads and validates this #World from a #WorldData source.
/// @param[out] world The state of the world to overwrite.
/// @param[in] data Geometry data to initialize @p world with.
/// @return @p true if @p data is valid and was loaded.
bool World_LoadData(World * const world, const WorldData * const data) {
	unsigned int x, y;
	WorldTile tile;
	bool startFound = false;
	bool rocketFound = false;

	world->data = data;
	if (data->timeLimit <= 0) {
		return false; // Impossible time limit
	}
	// Scan for required tiles
	for (y = 0; y < data->sizeX; ++y) {
		for (x = 0; x < data->sizeX; ++x) {
			tile = World_GetTileContents(world, x + 0.5f, y + 0.5f);
			if (tile == TILE_START) {
				if (startFound) {
					return false; // Multiple start positions not allowed
				}
				startFound = true;
				world->startX = x;
				world->startY = y;

			} else if (tile == TILE_ROCKET) {
				if (rocketFound) {
					return false; // Multiple rocket positions not allowed
				}
				rocketFound = true;
				world->rocketX = x;
				world->rocketY = y;

			} else if (tile == TILE_INVALID) {
				return false; // Unknown tile value
			}
		}
	}
	// Valid if all required locations found
	return startFound && rocketFound;
}

/// @brief Renders a static horizon to show behind walls and other level contents.
/// @param[in] world The loaded world to draw a background for.
/// @param[out] Buffer to draw the background to.
void World_DrawBackground(const World * const world, ScreenBuffer * const buffer) {
	// Columns before the speckle pattern advances.
	static const unsigned char speckleSpread = 3;
	// Offset to begin speckle pattern at.
	static const unsigned char speckleOffset = (speckleSpread - 1) / 2; // Centers pattern
	// Y-coordinate to draw the horizon at.
	static const unsigned int horizon = screenBufferHeight / 2;
	unsigned int x, y;

	// Plain black sky
	ScreenBuffer_RectangleSet(buffer, 0, 0, screenBufferWidth, horizon, true);
	// Speckled ground
	ScreenBuffer_RectangleSet(buffer, 0, horizon,
		screenBufferWidth, screenBufferHeight / 2, false);
	for (x = speckleOffset; x < screenBufferWidth; x += speckleSpread) {
		// Leave a 1-pixel gap between the horizon and speckled pattern
		for (y = horizon + 1 + (x / speckleSpread) % 2; y < screenBufferHeight; y += 2) {
			ScreenBuffer_PixelFill(buffer, x, y);
		}
	}
}

/// @brief Uses a modified line drawing algorithm to find wall collisions along a ray.
/// @param[in] world The loaded world to query.
/// @param[in] startX,startY Origin of the ray to trace from.
/// @param[in] angle Angle of the ray to trace towards, in radians.
/// @param[out] intersectDistance Final distance in tile lengths to a wall,
/// or @c INFINITY if no intersection was made.
/// @param[out] intersectTileX,intersectTileY Tile coordinate of the wall that
/// was intersected. Only set if an intersection occurs.
/// @param[out] intersectVertical @c true if the intersection was made on the
/// top or bottom of the given tile. Only set if an intersection occurs.
static bool _FindFirstIntersection(const World * const world,
	const float startX, const float startY, const float angle, float * const intersectDistance,
	unsigned int * const intersectTileX, unsigned int * const intersectTileY, bool * const intersectVertical
) {
	// Max distance in tile lengths to check for blocks.
	static const float farClip = 64.0;
	const float endX = startX + farClip * cosf(angle);
	const float endY = startY + farClip * sinf(angle);
	const float distanceX = fabsf(endX - startX);
	const float distanceY = fabsf(endY - startY);
	int x = (int)floorf(startX);
	int y = (int)floorf(startY);
	int incrementX, incrementY;
	unsigned int tileSteps = 1;
	float error, slope;
	bool movedVertically = false;

	if (distanceX == 0.0f) { // Vertical
		incrementX = 0;
		error = INFINITY;
	} else if (endX > startX) {
		incrementX = 1;
		tileSteps += (int)floorf(endX) - x;
		error = distanceY * (floorf(startX) + 1 - startX);
	} else {
		incrementX = -1;
		tileSteps += x - (int)floorf(endX);
		error = distanceY * (startX - floorf(startX));
	}

	if (distanceY == 0.0f) { // Horizontal
		incrementY = 0;
		error -= INFINITY;
	} else if (endY > startY) {
		incrementY = 1;
		tileSteps += (int)floorf(endY) - y;
		error -= distanceX * (floorf(startY) + 1 - startY);
	} else {
		incrementY = -1;
		tileSteps += y - (int)floorf(endY);
		error -= distanceX * (startY - floorf(startY));
	}

	// Walk through intersected tiles
	for (; tileSteps > 0; --tileSteps) {
		if (World_GetTileContents(world, x, y) == TILE_WALL) {
			*intersectTileX = x;
			*intersectTileY = y;
			*intersectVertical = movedVertically;

			// Calculate distance to actual intersection
			if (incrementX < 0) { // Leftward
				++x; // Hit right edge
			}
			if (incrementY < 0) { // Downward
				++y; // Hit top edge
			}
			if (distanceX == 0.0f) { // Purely vertical
				*intersectDistance = fabsf(y - startY);
				return true;
			} else if (distanceY == 0.0f) { // Purely horizontal
				*intersectDistance = fabsf(x- startX);
				return true;
			}
			slope = (endY - startY) / (endX - startX);
			if (movedVertically) { // Hit top or bottom
				*intersectDistance = hypotf((y - startY) / slope, y - startY);
			} else { // Hit left or right
				*intersectDistance = hypotf(x - startX, slope * (x - startX));
			}
			return true;
		}

		if (error > 0) {
			y += incrementY;
			error -= distanceX;
			movedVertically = true;
		} else {
			x += incrementX;
			error += distanceY;
			movedVertically = false;
		}
	}

	// Reached far clip without an intersection
	*intersectDistance = INFINITY;
	return false;
}

/// @brief Calculates the screen height and top of an object some distance away.
/// @param[in] objectDistance Distance from the eye to the object in tile lengths.
/// @param[in] objectHeight The height of the distant object in tile lengths.
/// @param[in] angle Relative angle from the eye's viewing angle to the object.
/// @param[out] projectedTop,projectedBottom Edges of the screen that the object should be drawn within.
inline static void _GetProjectedPosition(float objectDistance, const float objectHeight, const float angle,
	int * const projectedTop, int * const projectedBottom
) {
	float bottom;
	// Compensate for fish-eye effect
	objectDistance *= cosf(angle);
	if (objectDistance < 1.0f) { // Object will be larger than screen
		*projectedTop = 0;
		*projectedBottom = screenBufferHeight;
	} else {
		bottom = screenBufferHeight / 2 * (1.0f + 1.0f / objectDistance); // Un-rounded
		*projectedTop = bottom - screenBufferHeight * objectHeight / objectDistance + 0.5f;
		*projectedBottom = bottom + 0.5f;
	}
}

/// @return @c true if walls at the two coordinates are connected in a straight line.
/// @param[in] The loaded world to query.
/// @param[in] startX,startY The coordinate of the first wall tile, assumed to be a wall.
/// @param[in] endX,endY The coordinate of the last wall tile, assumed to be a wall.
inline static bool _AreWallsConnected(const World * const world,
	unsigned int startX, unsigned int startY,
	unsigned int endX, unsigned int endY
) {
	unsigned int index;
	if (startX == endX) { // Check for vertical line
		if (endY > startY) { // Swap for lowest to highest
			index = startY; startY = endY; endY = index;
		}
		// Check tiles between endpoints
		for (index = startY + 1; index < endY; ++index) {
			if (World_GetTileContents(world, startX, index) != TILE_WALL) {
				return false; // No need to also check for horizontal
			}
		}
		return true; // No breaks in between
	} else if (startY == endY) { // Check for horizontal line
		if (endX > startX) { // Swap for lowest to highest
			index = startX; startX = endX; endX = index;
		}
		// Check tiles between endpoints
		for (index = startX + 1; index < endX; ++index) {
			if (World_GetTileContents(world, index, startY) != TILE_WALL) {
				return false;
			}
		}
		return true; // No breaks in between
	}
	return false; // Points aren't orthogonal
}

/// @brief Renders the contents of this #World from the given perspective.
/// @param[in] world The loaded world to render.
/// @param[out] buffer The screen buffer to overlay game visuals on.
/// @param[in] eyeX,eyeY The world position to view from.
/// @param[in] eyeAngle The angle in radians to aim the view towards.
/// @param[in] eyeHeight The height of the camera, in tile lengths.
void World_DrawContents(const World * const world, ScreenBuffer * const buffer,
	const float eyeX, const float eyeY, const float eyeAngle, const float eyeHeight
) {
	static const float pi = 3.14159265358979323846f;
	// Height of the rocket, in tile lengths.
	static const float rocketHeight = 2.0f;
	// How many extra columns to draw to the left and right of the screen.
	static const int offScreenColumns = 1;
	// Distance from the player's eye to the screen, in wall lengths.
	static const float focalLength = 0.8f;
	// Height of all walls in tile lengths.
	static const float wallHeight = 1.0f;

	float intersectDistance, intersectDistanceLast = 0;
	unsigned int intersectTileX, intersectTileXLast = 0;
	unsigned int intersectTileY, intersectTileYLast = 0;
	bool intersectVertical, intersectVerticalLast = false;
	int lineTop, lineTopLast = 0;
	int lineBottom, lineBottomLast = 0;
	float columnCenter, columnAngle;
	const Bitmap *rocket;
	float rocketX, rocketY, rocketDistance, rocketAngle;
	int rocketLeft, rocketTop;
	bool rocketVisible = false;
	int x;

	// Draw rocket sprite behind all walls
	World_GetRocket(world, &rocketX, &rocketY);
	rocketAngle = atan2f(rocketY - eyeY, rocketX - eyeX) - eyeAngle;
	// Clamp angle between [-pi,pi]
	while (rocketAngle > pi) {
		rocketAngle -= 2 * pi;
	}
	while (rocketAngle < -pi) {
		rocketAngle += 2 * pi;
	}
	if (-pi / 2 < rocketAngle && rocketAngle < pi / 2) {
		// Rocket is in front of eye
		rocketDistance = hypotf(rocketX - eyeX, rocketY - eyeY) - focalLength / cosf(rocketAngle);
		_GetProjectedPosition(rocketDistance, rocketHeight, rocketAngle,
			&lineTop, &lineBottom);
		rocket = Sprites_GetRocketByHeight((unsigned int)(lineBottom - lineTop + 1));

		// Find center of rocket in pixel coordinates
		rocketX = screenBufferWidth / 2 - screenBufferWidth * focalLength * tanf(rocketAngle);
		if (0 < rocketX + rocket->width / 2 && rocketX - rocket->width / 2 < screenBufferWidth ) {
			// Rocket at least partially on-screen
			rocketVisible = true;
			rocketLeft = rocketX - rocket->width / 2 + 0.5f;
			rocketTop = lineBottom - (int)rocket->height + 1;
			ScreenBuffer_BitmapFill(buffer, rocketLeft, rocketTop, rocket);
		}
	}

	// Draw each column
	for (x = -offScreenColumns; x < (int)screenBufferWidth + offScreenColumns; ++x) {
		// Find angle from the eye to the center of this column on the screen
		columnCenter = (x + 0.5f) / screenBufferWidth - 0.5f;
		columnAngle = atan2f(-columnCenter, focalLength);

		// Trace ray looking for the first intersection
		if (_FindFirstIntersection(world, eyeX, eyeY, eyeAngle + columnAngle,
			&intersectDistance, &intersectTileX, &intersectTileY, &intersectVertical)
		) { // Intersected
			_GetProjectedPosition(intersectDistance, wallHeight, columnAngle,
				&lineTop, &lineBottom);
			ScreenBuffer_PixelFill(buffer, x, lineTop - 1);
			ScreenBuffer_RectangleSet(buffer, x, lineTop, 1, lineBottom - lineTop + 1, false);
			ScreenBuffer_PixelFill(buffer, x, lineBottom + 1);
			if (rocketVisible && rocketDistance < intersectDistance) {
				// Re-draw rocket over wall
				ScreenBuffer_BitmapFillColumn(buffer, x, rocketTop, x - rocketLeft, rocket);
			}

			if (x > -offScreenColumns) { // Last column's values available
				// Add wall borders with last column
				if (isinf(intersectDistanceLast)) { // Transition from nothing
					ScreenBuffer_RectangleSet(buffer, x - 1, lineTop, 1, lineBottom - lineTop + 1, true);
					if (rocketVisible && rocketDistance < intersectDistance) {
						// Re-draw rocket over border
						ScreenBuffer_BitmapFillColumn(buffer, x - 1, rocketTop, x - rocketLeft - 1, rocket);
					}
				} else if (intersectVertical != intersectVerticalLast // Wall side changed
					|| !_AreWallsConnected(world, intersectTileXLast, intersectTileYLast, intersectTileX, intersectTileY)
				) {
					// Draw line over more distant column, with height of the nearer
					if (intersectDistance > intersectDistanceLast) {
						ScreenBuffer_RectangleSet(buffer, x, lineTopLast, 1, lineBottomLast - lineTopLast + 1, true);
					} else {
						ScreenBuffer_RectangleSet(buffer, x - 1, lineTop, 1, lineBottom - lineTop + 1, true);
					}
				} else { // Same wall as last column
					// Draw borders along the sides of steep column height changes
					if (lineTop < lineTopLast - 1) { // Steep rise
						ScreenBuffer_RectangleSet(buffer, x - 1, lineTop, 1, lineTopLast - lineTop - 1, true);
					} else if (lineTopLast < lineTop - 1) { // Steep drop
						ScreenBuffer_RectangleSet(buffer, x, lineTopLast, 1, lineTop - lineTopLast - 1, true);
					}
					if (lineBottom < lineBottomLast - 1) { // Steep rise
						ScreenBuffer_RectangleSet(buffer, x, lineBottom + 2, 1, lineBottomLast - lineBottom - 1, true);
					} else if (lineBottomLast < lineBottom - 1) { // Steep drop
						ScreenBuffer_RectangleSet(buffer, x, lineBottomLast + 2, 1, lineBottom - lineBottomLast - 1, true);
					}
				}
			}
		} else { // No intersection
			if (x > -offScreenColumns) { // Last column's values available
				// Add wall borders with last column
				if (isfinite(intersectDistanceLast)) { // Transition to nothing
					ScreenBuffer_RectangleSet(buffer, x, lineTopLast, 1, lineBottomLast - lineTopLast + 1, true);
					if (rocketVisible && rocketDistance < intersectDistanceLast) {
						// Re-draw rocket over border
						ScreenBuffer_BitmapFillColumn(buffer, x, rocketTop, x - rocketLeft, rocket);
					}
				}
			}
		}

		// Keep previous intersection details for drawing wall borders
		intersectDistanceLast = intersectDistance;
		intersectTileXLast = intersectTileX;
		intersectTileYLast = intersectTileY;
		intersectVerticalLast = intersectVertical;
		lineTopLast = lineTop;
		lineBottomLast = lineBottom;
	}
}

/// @return The time limit of this #World in seconds.
/// @param[in] world The loaded world to query.
float World_GetTimeLimit(const World * const world) {
	return world->data->timeLimit;
}

/// @brief Gets the #Player starting position within this loaded #World.
/// @param[in] world The loaded world to query.
/// @param[out] x,y Will be assigned the starting tile's center position.
void World_GetStart(const World * const world, float * const x, float * const y) {
	*x = world->startX + 0.5f;
	*y = world->startY + 0.5f;
}

/// @brief Gets the position of the rocket within this loaded #World.
/// @param[in] world The loaded world to query.
/// @param[out] x,y Will be assigned the rocket tile's center position.
void World_GetRocket(const World * const world, float * const x, float * const y) {
	*x = world->rocketX + 0.5f;
	*y = world->rocketY + 0.5f;
}

/// @return The type of tile present at the given coordinate.
/// @param[in] world The loaded world to query.
/// @param[in] x,y Tile position to get the contents of.
WorldTile World_GetTileContents(const World * const world, const float x, const float y) {
	// World data tile characters
	static const char byteSpace = ' ';
	static const char byteWall = '#';
	static const char byteStart = 'S';
	static const char byteRocket = 'R';
	static const char byteExit = '@';

	unsigned int dataX, dataY;
	if (x < 0 || world->data->sizeX <= x
		|| y < 0 || world->data->sizeY <= y
	) { // Outside of level
		return TILE_SPACE;
	}

	// Convert Cartesian world coordinates to data coordinates
	dataX = x; // Truncate
	dataY = world->data->sizeY - (unsigned int)y - 1; // Byte data's Y-axis increases downwards
	switch (world->data->contents[dataY * world->data->sizeX + dataX]) {
	case byteSpace:
		return TILE_SPACE;
	case byteWall:
		return TILE_WALL;
	case byteStart:
		return TILE_START;
	case byteRocket:
		return TILE_ROCKET;
	case byteExit:
		return TILE_EXIT;
	default:
		return TILE_INVALID;
	}
}

/// @brief Look-up table for tile types that can be walked through.
static const bool _tileTraversability[] = {
	[TILE_INVALID] = true,
	[TILE_SPACE] = true,
	[TILE_WALL] = false,
	[TILE_START] = true,
	[TILE_ROCKET] = false,
	[TILE_EXIT] = true
};
/// @return @c true if the given tile can be walked through by a player.
/// @param[in] tile The tile type to query.
bool World_IsTileTraversible(const WorldTile tile) {
	return _tileTraversability[tile];
}
