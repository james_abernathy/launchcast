/// @file
/// @brief Methods for reading input from hardware controls.

#include "Controls.h"
#include <math.h>
#include <stdbool.h>
#include "FrameTimer.h"
#include "tm4c123gh6pm.h"

/// @brief Up button input pin mask.
static const unsigned char _upMask = 0x01; // PE0
/// @brief Down button input pin mask.
static const unsigned char _downMask = 0x02; // PE1
/// @brief Maximum possible value that the slider can output.
static const unsigned long _sliderMax = ADC_SSFIFO3_DATA_M;

/// @brief Configurable center point, or zero, of the slider.
static unsigned long _sliderCenter = 0;

/// @brief Initializes this #ButtonState to track the next complete press of a button.
/// @param[out] state The state to initialize.
/// @param[in] pressed The initial state of the tracked button.
void ButtonState_Initialize(ButtonState * const state, const bool pressed) {
	state->released = !pressed;
	state->pressed = false;
}

/// @brief Tracks a complete button press, through its release.
/// @param[in,out] state The past #ButtonState to update.
/// @param[in] pressed The new state of the tracked button.
/// @return @c true if the latest update has completed a full button press and release cycle.
bool ButtonState_PressCompleted(ButtonState * const state, const bool pressed) {
	if (!state->released) { // Started out held down
		state->released = !pressed;
	} else if (!state->pressed) { // Waiting for initial press
		state->pressed = pressed;
	} else if (!pressed) { // Waiting for release
		return true; // Press complete
	}
	return false;
}

/// @brief Initializes button input hardware.
static void _ButtonsInitialize(void) {
	static const unsigned char mask = _upMask | _downMask;
	volatile unsigned long delay;

	SYSCTL_RCGC2_R |= SYSCTL_RCGC2_GPIOE; // Enable clock
	delay = SYSCTL_RCGC2_R; // Wait for clock to stabilize
	GPIO_PORTE_AMSEL_R &= ~mask; // Disable analog function
	GPIO_PORTE_PCTL_R &= ~(GPIO_PCTL_PE0_M | GPIO_PCTL_PE1_M);
	GPIO_PORTE_DIR_R &= ~mask; // Inputs
	GPIO_PORTE_PDR_R |= mask; // Use pull-down resistors
	GPIO_PORTE_AFSEL_R &= ~mask; // No alternate function
	GPIO_PORTE_DEN_R |= mask; // Enable pins
}

/// @brief Initializes the slider hardware for analog input.
static void _SliderInitialize(void) {
	static const unsigned char ain1Mask = 0x04;
	volatile unsigned long delay;

	// GPIO pin initialization
	SYSCTL_RCGC2_R |= SYSCTL_RCGC2_GPIOE; // Enable clock
	delay = SYSCTL_RCGC2_R; // Wait for clock to stabilize
	GPIO_PORTE_DIR_R &= ~ain1Mask; // Input
	GPIO_PORTE_AFSEL_R |= ain1Mask; // Enable alternate function
	GPIO_PORTE_DEN_R &= ~ain1Mask; // Disable digital I/O
	GPIO_PORTE_AMSEL_R |= ain1Mask; // Enable analog function

	// ADC initialization
	SYSCTL_RCGC0_R |= SYSCTL_RCGC0_ADC0; // Activate ADC
	delay = SYSCTL_RCGC2_R; // Wait for clock to stabilize
	SYSCTL_RCGC0_R &= ~SYSCTL_RCGC0_ADC0SPD_M;
	SYSCTL_RCGC0_R |= SYSCTL_RCGC0_ADC0SPD_125K; // Max 125kHz sampling
	ADC0_SSPRI_R = 0x0123; // Sequencer 3 is highest priority
	ADC0_ACTSS_R &= ~ADC_ACTSS_ASEN3; // Disable sample sequencer
	ADC0_EMUX_R &= ~ADC_EMUX_EM3_M; // Seq3 is software trigger
	ADC0_SSMUX3_R &= ~ADC_SSMUX3_MUX0_M; // Clear SS3 field
	ADC0_SSMUX3_R += 1; // Set channel Ain1 (PE2)
	// Enable interrupts and set end of sequence
	ADC0_SSCTL3_R = ADC_SSCTL3_IE0 | ADC_SSCTL3_END0;
	ADC0_ACTSS_R |= ADC_ACTSS_ASEN3; // Enable sample sequencer
}

/// @brief Samples the slider's raw position value.
/// @note Blocks while sampling.
/// @return A 12-bit ADC value.
static unsigned long _SliderSample(void) {
	unsigned long result;
	ADC0_PSSI_R = ADC_PSSI_SS3; // Initiate sampling
	while(!(ADC0_RIS_R & ADC_RIS_INR3)) { // Not done
		// Wait for conversion to finish
	}
	result = ADC0_SSFIFO3_R & ADC_SSFIFO3_DATA_M; // Read 12-bit result
	ADC0_ISC_R = ADC_RIS_INR3; // Acknowledge completion
	return result;
}

/// @brief Initializes hardware resources used for input controls.
void Controls_Initialize(void) {
	FrameTimer_Initialize(); // Used for button debouncing
	_ButtonsInitialize();
	_SliderInitialize();
	Controls_ZeroSlider();
}

/// @return @c true if the up button is held down.
bool Controls_IsUpPressed(void) {
	return (GPIO_PORTE_DATA_R & _upMask) == _upMask;
}

/// @return @c true if the down button is held down.
bool Controls_IsDownPressed(void) {
	return (GPIO_PORTE_DATA_R & _downMask) == _downMask;
}

/// Waits until either button is pressed and then released before returning.
void Controls_WaitForButtonPress(void) {
	ButtonState upState, downState;
	ButtonState_Initialize(&upState, Controls_IsUpPressed());
	ButtonState_Initialize(&downState, Controls_IsDownPressed());
	while (true) {
		if (ButtonState_PressCompleted(&upState, Controls_IsUpPressed())
			|| ButtonState_PressCompleted(&downState, Controls_IsDownPressed())
		) {
			return;
		}
		// Allow button debounce between frames
		FrameTimer_WaitUntilRefresh();
	}
}

/// @return The slider's current offset from its center, without applying deadzone.
/// @note Negative values are left of center.
long Controls_GetSliderOffset(void) {
	return (signed long)_SliderSample() - _sliderCenter;
}

/// @brief Sets the slider's center point at its current position.
void Controls_ZeroSlider(void) {
	_sliderCenter = _SliderSample();
}

/// @brief Applies and eliminates the slider's deadzone.
/// @param[in] value A raw sample from the slider.
/// @return Zero for positions within the deadzone, and re-scaled values beyond it.
static long _SliderApplyDeadzone(long value) {
	static const unsigned char deadzonePercent = 10;
	static const long maxSigned = _sliderMax / 2;
	static const long deadzone = maxSigned * deadzonePercent / 100;
	if (value > deadzone) {
		value -= deadzone;
	} else if (value < -deadzone) {
		value += deadzone;
	} else { // Within deadzone
		return 0;
	}
	// Scale values outside deadzone across full slider range
	return (value * maxSigned) / (maxSigned - deadzone);
}

/// @return The commanded forward velocity depending on the up and down buttons, in tiles per second.
/// @note When both are pressed, they cancel each other out.
float Controls_GetForwardVelocity(void) {
	static const float forwardVelocity = 1.0f; // Tiles per second
	static const float backwardVelocity = -forwardVelocity * 0.6f;
	const bool up = Controls_IsUpPressed();
	const bool down = Controls_IsDownPressed();
	if (up != down) { // Moving
		return up ? forwardVelocity : backwardVelocity;
	}
	// None or both pressed
	return 0;
}

/// @return The commanded rate of rotation from the slider, in radians per second.
float Controls_GetRotationRate(void) {
	static const float pi = 3.14159265358979323846f;
	static const float maxRate = pi; // Radians per second
	static const long maxSigned = _sliderMax / 2;
	const float rate = -(float)_SliderApplyDeadzone(Controls_GetSliderOffset()) / maxSigned * maxRate;
	// Clamp to bounds for when slider's centering allows extra travel
	return fminf(fmaxf(rate, -maxRate), maxRate);
}
