/// @file
/// @brief Animation shown after the player wins.

#ifndef _WINSCREEN_H
#define _WINSCREEN_H

#include "ScreenBuffer.h"

void WinScreen_Show(ScreenBuffer * const buffer);

#endif // !defined(_WINSCREEN_H)
