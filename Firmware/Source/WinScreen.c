/// @file
/// @brief Animation shown after the player wins.

#include "WinScreen.h"
#include <stdbool.h>
#include "Bitmap.h"
#include "FrameTimer.h"
#include "Screen.h"
#include "ScreenBuffer.h"
#include "Sound.h"
#include "SoundSamples.h"
#include "Sprites.h"

/// @brief Shows an un-skippable victory animation.
/// @param[out] buffer Shared #ScreenBuffer to use for rendering.
void WinScreen_Show(ScreenBuffer * const buffer) {
	// How many frame updates to wait with the hatch open.
	static const unsigned int delayFrames = 0.5f * frameTimerFrequency;
	// How many frame updates the hatch takes to close before sealing.
	static const unsigned int hatchCloseFrames = 2.0f * frameTimerFrequency;
	// How many frames to delay before the hatch completely seals.
	static const unsigned int hatchLockFrames = 1.0f * frameTimerFrequency;
	// How many frames to delay after the hatch seals.
	static const unsigned int hatchPauseFrames = 0.5f * frameTimerFrequency;
	// How many frame updates taken to zoom out from the rocket.
	static const unsigned int zoomFrames = 3.0f * frameTimerFrequency;
	// Frames to delay between countdown ticks/text lines.
	static const unsigned int countdownIntervalFrames = 1.0f * frameTimerFrequency;
	// How many frames the rocket takes to accelerate completely off-screen.
	static const unsigned int launchFrames = 4.0f * frameTimerFrequency;
	// How many frames the invader takes to intersect with the launching rocket before exploding.
	static const unsigned int launchInvaderFrames = 2.0f * frameTimerFrequency;

	// Offset of the hatch's upper-left corner from that of the rocket.
	static const int hatchOffsetX = 15;
	static const int hatchOffsetY = 16;
	// Center X-coordinate where the rocket launches from.
	static const int launchX = 17;
	// Position of the first text line's top-left corner.
	static const int textLeft = 30;
	static const int textTop = 3;
	static const int textLineHeight = 12;

	// Number of frames between invader animation stages.
	static const unsigned int invaderFramePeriod = 15;
	// Starting and ending coordinates of the invader during lift-off.
	static const int invaderStartLeft = -13;
	static const int invaderStartTop = 5;
	static const int invaderEndLeft = 6;
	static const int invaderEndTop = 10;

	// Tones to use during countdown
	static const float countdownWaitFrequency = 220.0f;
	static const float countdownGoFrequency = countdownWaitFrequency * 2;

	// Initial "depth" of the rocket before zooming out.
	static const float initialZ = 1.0f;
	static const int centerX = screenBufferWidth / 2;

	const Bitmap *rocket = Sprites_GetRocketByHeight(screenBufferHeight);
	const Bitmap *invader;
	const unsigned int initialHeight = rocket->height;
	// Distance required to zoom from maximum- to minimum-sized sprites
	const float finalZ = (float)initialHeight / Sprites_GetRocketByHeight(0)->height;
	// High enough for the rocket and its trail to pass off-screen
	const unsigned int launchHeight = screenBufferHeight + spriteRocketTrail.height;
	unsigned int frameCount;
	int rocketLeft, rocketTop;
	int invaderLeft, invaderTop;
	float rocketZ, progress;

	ScreenBuffer_Clear(buffer);
	// Center rocket at bottom
	rocketLeft = centerX - (int)rocket->width / 2;
	rocketTop = (int)screenBufferHeight - (int)rocket->height;
	ScreenBuffer_BitmapFill(buffer, rocketLeft, rocketTop, rocket);
	ScreenBuffer_BitmapFill(buffer, rocketLeft + hatchOffsetX, rocketTop + hatchOffsetY,
		&spriteRocketHatchFrames[0]); // Start with hatch open

	// Pause with hatch open
	Screen_DrawBuffer(buffer);
	FrameTimer_WaitFrames(delayFrames);
	// Close the hatch
	for (frameCount = 0; frameCount < hatchCloseFrames; ++frameCount) {
		ScreenBuffer_BitmapFill(buffer, rocketLeft + hatchOffsetX, rocketTop + hatchOffsetY,
			&spriteRocketHatchFrames[frameCount * spriteRocketHatchFrameCount / hatchCloseFrames]);
		Screen_DrawBuffer(buffer);
		FrameTimer_WaitUntilRefresh();
	}
	// Wait before and after locking it
	FrameTimer_WaitFrames(hatchLockFrames);
	ScreenBuffer_BitmapFill(buffer, rocketLeft, rocketTop, rocket);
	Screen_DrawBuffer(buffer);
	FrameTimer_WaitFrames(hatchPauseFrames);

	// Zoom far away
	for (frameCount = 0; frameCount < zoomFrames; ++frameCount) {
		rocketZ = initialZ + (finalZ - initialZ) * frameCount / zoomFrames;
		rocket = Sprites_GetRocketByHeight(initialHeight / rocketZ + 0.5f); // Perspective
		rocketLeft = centerX - (int)rocket->width / 2 + (int)frameCount * (launchX - centerX) / (int)zoomFrames;
		rocketTop = (int)screenBufferHeight - (int)rocket->height;
		ScreenBuffer_BitmapFill(buffer, rocketLeft, rocketTop, rocket);
		Screen_DrawBuffer(buffer);
		FrameTimer_WaitUntilRefresh();
		ScreenBuffer_RectangleSet(buffer,
			rocketLeft, rocketTop, rocket->width, rocket->height, false);
	}
	// Place at final launch position
	rocket = Sprites_GetRocketByHeight(initialHeight / finalZ + 0.5f);
	rocketLeft = launchX - (int)rocket->width / 2;
	rocketTop = (int)screenBufferHeight - (int)rocket->height;
	ScreenBuffer_BitmapFill(buffer, rocketLeft, rocketTop, rocket);

	// Count down to lift-off
	ScreenBuffer_BitmapFill(buffer,
		textLeft, textTop + textLineHeight * 0, &spriteWinTextLaunch);
	Screen_DrawBuffer(buffer);
	Sound_PlayFrequency(countdownWaitFrequency);
	FrameTimer_WaitFrames(countdownIntervalFrames / 2);
	Sound_Stop();
	FrameTimer_WaitFrames(countdownIntervalFrames / 2);
	ScreenBuffer_BitmapFill(buffer,
		textLeft, textTop + textLineHeight * 1, &spriteWinTextPad);
	Screen_DrawBuffer(buffer);
	Sound_PlayFrequency(countdownWaitFrequency);
	FrameTimer_WaitFrames(countdownIntervalFrames / 2);
	Sound_Stop();
	FrameTimer_WaitFrames(countdownIntervalFrames / 2);
	ScreenBuffer_BitmapFill(buffer,
		textLeft, textTop + textLineHeight * 2, &spriteWinTextLift);
	// Add dust and vibrate
	ScreenBuffer_BitmapFill(buffer,
		launchX - (int)spriteRocketDust.width / 2,
		screenBufferHeight - spriteRocketDust.height, &spriteRocketDust);
	Sound_PlayFrequency(countdownWaitFrequency);
	for (frameCount = 0; frameCount < countdownIntervalFrames; ++frameCount) {
		if (frameCount == countdownIntervalFrames / 2) { // Stop tone
			Sound_Stop();
		}
		ScreenBuffer_BitmapFill(buffer, rocketLeft + (int)(frameCount % 2), rocketTop, rocket);
		Screen_DrawBuffer(buffer);
		FrameTimer_WaitUntilRefresh();
		ScreenBuffer_RectangleSet(buffer, rocketLeft, rocketTop, rocket->width + 1, rocket->height, false);
	}
	ScreenBuffer_BitmapFill(buffer,
		textLeft, textTop + textLineHeight * 3, &spriteWinTextOff);

	// Take off and take down invader
	Sound_PlayFrequency(countdownGoFrequency);
	for (frameCount = 0; frameCount < launchFrames; ++frameCount) {
		progress = (float)frameCount / launchFrames;
		// Accelerate to launchHeight in given time
		rocketTop = (int)screenBufferHeight - (int)rocket->height
			- (int)launchHeight * progress * progress;

		if (frameCount < launchInvaderFrames) {
			// Invader flies in the path of the rocket
			invader = &spriteInvaderFrames[(frameCount / invaderFramePeriod) % spriteInvaderFrameCount];
			invaderLeft = invaderStartLeft + (int)frameCount * (invaderEndLeft - invaderStartLeft) / (int)launchInvaderFrames;
			invaderTop = invaderStartTop + (int)frameCount * (invaderEndTop - invaderStartTop) / (int)launchInvaderFrames;
			ScreenBuffer_BitmapFill(buffer, invaderLeft, invaderTop, invader);
		} else { // Invader destroyed
			if (frameCount == launchInvaderFrames) { // Just blew up
				// Stop launch tone as well
				Sound_PlaySample(&soundExplosion, 1);
			}
			ScreenBuffer_BitmapFill(buffer,
				invaderEndLeft + ((int)invader->width - (int)spriteExplosion.width) / 2,
				invaderEndTop + ((int)invader->height - (int)spriteExplosion.height) / 2, &spriteExplosion);
		}
		ScreenBuffer_BitmapFill(buffer, rocketLeft, rocketTop, rocket);
		ScreenBuffer_BitmapFill(buffer,
			launchX - (int)spriteRocketTrail.width / 2,
			rocketTop + (int)rocket->height, &spriteRocketTrail);
		Screen_DrawBuffer(buffer);
		FrameTimer_WaitUntilRefresh();
		// Erase moving sprites
		if (frameCount < launchInvaderFrames) { // Invader alive
			ScreenBuffer_RectangleSet(buffer, invaderLeft, invaderTop,
				invader->width, invader->height, false);
		}
		ScreenBuffer_RectangleSet(buffer,
			launchX - (int)spriteRocketTrail.width / 2, 0,
			spriteRocketTrail.width, screenBufferHeight, false);
	}
}
