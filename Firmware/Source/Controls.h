/// @file
/// @brief Methods for reading input from hardware controls.

#ifndef _CONTROLS_H
#define _CONTROLS_H

#include <stdbool.h>

/// @brief State of a button to track complete presses.
typedef struct ButtonState {
	bool released; ///< @c false until the user releases their last button press, which will be ignored.
	bool pressed; ///< @c true once the button has been pressed.
} ButtonState;

void ButtonState_Initialize(ButtonState * const state, const bool pressed);
bool ButtonState_PressCompleted(ButtonState * const state, const bool pressed);
void Controls_Initialize(void);
bool Controls_IsUpPressed(void);
bool Controls_IsDownPressed(void);
void Controls_WaitForButtonPress(void);
long Controls_GetSliderOffset(void);
void Controls_ZeroSlider(void);
float Controls_GetForwardVelocity(void);
float Controls_GetRotationRate(void);

#endif // !defined(_CONTROLS_H)
