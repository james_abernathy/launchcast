/// @file
/// @brief #WorldData structures for the game to choose from.

#include "Worlds.h"
#include <stddef.h>
#include "World.h"

/// @brief Collection of all available worlds that can be played.
const WorldData worlds[] = {
	{.sizeX = 24, .sizeY = 23, .timeLimit = 120.0f, .contents =
		" ###################    "
		"# ##     ###        #   "
		"#    ###     ### ## #   "
		"# ###   ### #   #   #   "
		"#     #     # ### ##    "
		"###### # #### #         "
		"#      #      #         "
		"# # ######### #         "
		"# #           #         "
		"# # ###  ######         "
		"###   #  # #####   @@@  "
		"#    ##  ##     # @@@@@#"
		"# # #       S   # @@R@@#"
		"### #  ####     # @@@@@#"
		"#   #  #   #####   @@@# "
		" ####  ##          ###  "
		"  #      #              "
		" #        #             "
		"#    ##    #            "
		"#    ##    #            "
		" #        #             "
		"  #      #              "
		"   ######               "
	}
};

/// @brief How many worlds are available to play.
const size_t worldCount = sizeof(worlds) / sizeof(worlds[0]);
