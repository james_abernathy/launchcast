/// @file
/// @brief Frame rate timing and other time-related routines.

#ifndef _FRAMETIMER_H
#define _FRAMETIMER_H

#include <stddef.h>

/// @brief Frames per second that the framerate timer will regulate to.
static const float frameTimerFrequency = 30.0f; // Hz
/// @brief The length of the time string generated by FrameTimer_FormatTime, not including a null terminator.
static const size_t frameTimerTextLength = 4;

void FrameTimer_Initialize(void);
void FrameTimer_WaitUntilRefresh(void);
void FrameTimer_WaitFrames(const unsigned int numFrames);
char *FrameTimer_FormatTime(const float seconds, char *buffer);

#endif // !defined(_FRAMETIMER_H)
