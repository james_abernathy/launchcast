/// @file
/// @brief Routines for controlling the LEDs on the game box.

#ifndef _LIGHTS_H
#define _LIGHTS_H

#include <stdbool.h>

void Lights_Initialize(void);
bool Lights_IsTopLit(void);
bool Lights_IsBottomLit(void);
void Lights_SetTop(const bool enable);
void Lights_SetBottom(const bool enable);

#endif // !defined(_LIGHTS_H)
