/// @file
/// @brief Sound generation driver.

#include "Sound.h"
#include <stddef.h>
#include "Lights.h"
#include "tm4c123gh6pm.h"

/// @brief Output pin mask for DAC bits.
static const unsigned char _dacMask = 0x0F; // PB0-PB3

/// @brief The shape of one period of the generated tone.
static const unsigned char _waveformLoop[] = {
	// 4-bit DAC values representing a sinusoid.
	// First sample should start at equilibrium.
	8, 10, 13, 14, 15, 14, 13, 10,
	8,  5,  2,  1,  0,  1,  2,  5
};
/// @brief The number of samples within _waveformLoop before it should repeat.
static const size_t _waveformSamples = sizeof(_waveformLoop) / sizeof(_waveformLoop[0]);

/// @brief Next index into _waveformLoop or _sampleActive to output by the DAC.
static size_t _waveformIndex = 0;
/// @brief The currently playing sound sample, or @c NULL if a frequency is playing instead.
static const SoundSample *_sampleActive = 0;
/// @brief The remaining number of _sampleActive to play.
static unsigned int _sampleRepetitions = 0;

/// @brief Sets the DAC's analog output to a 4-bit value.
/// @param[in] data The digital value to output.
inline static void _DacOutput(const unsigned char data) {
	// High values will turn on an LED, for visualizing sound output.
	static const unsigned char lightThreshold = 14;
	// Sets masked bits without changing others in an atomic write
	GPIO_PORTB_DATA_R ^= (data ^ GPIO_PORTB_DATA_R) & _dacMask;
	Lights_SetBottom(data >= lightThreshold);
}

/// @brief Initializes the 4-bit DAC outputs.
static void _DacInitialize(void) {
	volatile unsigned long delay;

	SYSCTL_RCGC2_R |= SYSCTL_RCGC2_GPIOB; // Enable clock
	delay = SYSCTL_RCGC2_R;
	GPIO_PORTB_AMSEL_R &= ~_dacMask; // Disable analog function
	GPIO_PORTB_PCTL_R &= ~(
		GPIO_PCTL_PB3_M | GPIO_PCTL_PB2_M | GPIO_PCTL_PB1_M | GPIO_PCTL_PB0_M);
	GPIO_PORTB_DIR_R |= _dacMask; // Outputs
	//GPIO_PORTB_DR8R_R |= _dacMask; // Drive 8mA out
	GPIO_PORTB_AFSEL_R &= ~_dacMask; // No alternate functions
	GPIO_PORTB_DEN_R |= _dacMask; // Enable pins

	_DacOutput(0);
}

/// @brief Enables the sound generation interrupt.
static void _TimerEnable(void) {
	TIMER2_CTL_R |= TIMER_CTL_TAEN;
}

/// @brief Disables the sound generation interrupt.
static void _TimerDisable(void) {
	TIMER2_CTL_R &= ~TIMER_CTL_TAEN;
}

/// @brief Initializes sound hardware.
void Sound_Initialize(void) {
	static const unsigned char priority = 0; // Lower values are higher priority
	volatile unsigned long delay;
	Lights_Initialize();
	_DacInitialize();

	// Timer setup
	SYSCTL_RCGCTIMER_R |= SYSCTL_RCGCTIMER_R2; // Enable clock
	delay = SYSCTL_RCGCTIMER_R; // Wait for clock to stabilize
	_TimerDisable();
	TIMER2_CFG_R = TIMER_CFG_32_BIT_TIMER;
	TIMER2_TAMR_R = TIMER_TAMR_TAMR_PERIOD; // Repeat periodically

	TIMER2_TAPR_R = 0; // Disable pre-scaler
	TIMER2_ICR_R |= TIMER_ICR_TATOCINT; // Clear timeout flag
	TIMER2_IMR_R = TIMER_IMR_TATOIM; // Arm interrupt
	// Set priority
	NVIC_PRI5_R &= ~NVIC_PRI5_INT23_M;
	NVIC_PRI5_R |= priority << NVIC_PRI5_INT23_S;
	// Vector number 39, interrupt number 23
	NVIC_EN0_R = 1 << 23;
	Sound_Stop();
}

/// @brief Stop generating all sound.
void Sound_Stop(void) {
	_TimerDisable();

	_sampleActive = 0; // Default to tone generation instead of sample data
	// Rest at equilibrium (middle of waveform)
	_waveformIndex = 0;
	_DacOutput(_waveformLoop[_waveformIndex]);
}

/// @brief Begins firing the timer interrupt at a given speed.
/// @param[in] frequency Frequency of interrupts, in Hertz.
static void _SetTimerFrequency(const float frequency) {
	static const unsigned long clockFrequency = 80000000L; // 80 MHz
	unsigned long ticksPerInterrupt;
	if (frequency <= 0) { // Impossible
		Sound_Stop();
		return;
	}
	ticksPerInterrupt = clockFrequency / frequency + 0.5f; // Round
	if (ticksPerInterrupt < 2 // Too fast for timer
		|| ticksPerInterrupt > TIMER_TAILR_M - 1 // Too slow for timer
	) {
		Sound_Stop();
		return;
	}

	_TimerDisable();
	// Automatically resets timer
	TIMER2_TAILR_R = ticksPerInterrupt - 1;
	_TimerEnable(); // Begin firing interrupt again
}

/// @brief Begins generating a sine wave.
/// @param[in] frequency Frequency of the waveform to generate, in Hertz.
void Sound_PlayFrequency(const float frequency) {
	if (_sampleActive) {
		Sound_Stop();
	}
	_SetTimerFrequency(frequency * _waveformSamples);
}

/// @brief Begins playback of a sampled sound.
/// @param[in] sample The sound data to play.
/// @param[in] repetitions How many times to repeat the sound sample back to back.
void Sound_PlaySample(const SoundSample * const sample, const unsigned int repetitions) {
	// Sampling frequency expected of all waveform data
	static const float samplingFrequency = 11025.0f;
	Sound_Stop();
	if (!sample->length || !repetitions) {
		return; // Nothing to play
	}
	_sampleActive = sample;
	_sampleRepetitions = repetitions - 1;
	_SetTimerFrequency(samplingFrequency);
}

// Timer interrupt service routine to update sound wave.
void Timer2A_Handler(void) {
	TIMER2_ICR_R |= TIMER_ICR_TATOCINT; // Acknowledge timeout
	if (_sampleActive) {
		if (_waveformIndex >= _sampleActive->length) { // Sample done
			if (!_sampleRepetitions) { // Finished repeating
				Sound_Stop();
				return;
			} else { // Play again
				--_sampleRepetitions;
				_waveformIndex = 0;
			}
		}
		// Update sample
		if (_waveformIndex % 2) { // Lower nibble
			_DacOutput(0x0F & _sampleActive->data[_waveformIndex++ / 2]);
		} else { // Upper nibble
			_DacOutput(_sampleActive->data[_waveformIndex++ / 2] >> 4);
		}
	} else { // Playing a frequency
		if (_waveformIndex >= _waveformSamples) {
			_waveformIndex = 0; // Repeat waveform
		}
		_DacOutput(_waveformLoop[_waveformIndex++]);
	}
}
