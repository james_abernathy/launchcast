/// @file
/// @brief Animation shown after the player loses.

#ifndef _LOSESCREEN_H
#define _LOSESCREEN_H

#include "ScreenBuffer.h"

void LoseScreen_Show(ScreenBuffer * const buffer);

#endif // !defined(_LOSESCREEN_H)
