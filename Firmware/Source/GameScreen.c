/// @file
/// @brief Actual gameplay screen for the player to interact with.

#include "GameScreen.h"
#include <stdbool.h>
#include "Controls.h"
#include "FrameTimer.h"
#include "Lights.h"
#include "Player.h"
#include "Screen.h"
#include "ScreenBuffer.h"
#include "Sound.h"
#include "SoundSamples.h"
#include "World.h"

/// @brief Renders a countdown timer at the top of the screen.
/// @param[out] Gameplay screen to render the time over.
/// @param[in] secondsRemaining Number of seconds left before the game ends.
static void _DrawTimeOverlay(ScreenBuffer * const buffer, const float secondsRemaining) {
	// Pixel width of black border surrounding text.
	static const unsigned char borderSize = 1;
	static const unsigned int textWidth = frameTimerTextLength * screenBufferGlyphWidth;
	static const int textLeft = ((int)screenBufferWidth - (int)textWidth) / 2;

	char timeText[frameTimerTextLength + 1];
	// Add black background to form border
	ScreenBuffer_RectangleSet(buffer, textLeft - borderSize, 0,
		textWidth + borderSize * 2, screenBufferGlyphHeight + borderSize * 2, true);
	FrameTimer_FormatTime(secondsRemaining, timeText);
	ScreenBuffer_TextFill(buffer, textLeft, borderSize, timeText);
}

/// @return A multiplier for clock tick sounds depending on time left.
/// @param[in] secondsRemaining How many seconds the player has left.
inline static unsigned int _GetClockMultiplier(const float secondsRemaining) {
	// Time limits where the clock doubles its pace.
	static const float clockRushTime1 = 60.0f;
	static const float clockRushTime2 = 10.0f;
	static const float clockRushTime4 = 6.0f;
	static const float clockRushTime8 = 1.0f;

	if (secondsRemaining > clockRushTime1) {
		return 1;
	} else if (secondsRemaining > clockRushTime2) {
		return 2;
	} else if (secondsRemaining > clockRushTime4) {
		return 4;
	} else if (secondsRemaining > clockRushTime8) {
		return 8;
	} else {
		return 16;
	}
}

/// @brief Plays the actual game!
/// @param[out] buffer Shared #ScreenBuffer to use for rendering.
/// @param[in] worldData Level geometry to load and play on.
/// @return The number of seconds the player had to spare, or @c 0 if they ran out of time.
float GameScreen_Show(ScreenBuffer * const buffer, const WorldData * const worldData) {
	static const float frameTime = 1.0f / frameTimerFrequency;
	// Max frames between clock tick tock sounds.
	static const unsigned long clockTickInterval = 4 * frameTimerFrequency + 0.5f;
	static ScreenBuffer background;
	World world;
	Player player;
	unsigned long framesMax, framesRemaining = 0;
	float secondsRemaining;
	bool clockTicked = false;

	if (!World_LoadData(&world, worldData)) { // Invalid level data
		return 0.0f; // Lose for lack of a better option
	}
	framesMax = World_GetTimeLimit(&world) / frameTime + 0.5f;
	Player_EnterWorld(&player, &world);
	// Render static sky/floor/horizon
	World_DrawBackground(&world, &background);

	for (framesRemaining = framesMax; framesRemaining > 0; --framesRemaining) {
		secondsRemaining = frameTime * framesRemaining;
		// Blink light with timer
		Lights_SetTop((unsigned int)secondsRemaining % 2);
		// Play tick tock sound
		if (framesRemaining % (clockTickInterval / _GetClockMultiplier(secondsRemaining)) == 0) {
			Sound_PlaySample(clockTicked ? &soundClockTock : &soundClockTick, 1);
			clockTicked = !clockTicked;
		}

		// Update controls and move
		Player_SetForwardVelocity(&player, Controls_GetForwardVelocity());
		Player_SetRotationRate(&player, Controls_GetRotationRate());
		Player_Update(&player, frameTime);
		if (Player_HasWon(&player)) {
			return secondsRemaining;
		}

		ScreenBuffer_Copy(buffer, &background);
		Player_DrawPerspective(&player, buffer);
		_DrawTimeOverlay(buffer, secondsRemaining);

		Screen_DrawBuffer(buffer);
		FrameTimer_WaitUntilRefresh();
	}
	// Ran out of time
	return 0.0f;
}
