/// @file
/// @brief #Player game object and routines for manipulating it.

#include "Player.h"
#include <math.h>
#include <stdbool.h>
#include "ScreenBuffer.h"
#include "World.h"

/// @brief Height of the player in tile lengths.
static const float playerHeight = 0.6;

/// @brief Initializes this #Player at the starting point within a #World.
/// @param[out] player The player to initialize.
/// @param[in] world The world to place this #Player within.
void Player_EnterWorld(Player * const player, const World * const world) {
	float rocketX, rocketY;
	player->world = world;
	player->distanceTravelled = 0.0f;
	World_GetStart(world, &player->x, &player->y);
	// Face player towards goal initially
	World_GetRocket(world, &rocketX, &rocketY);
	player->rotation = atan2f(rocketY - player->y, rocketX - player->x);

	Player_SetRotationRate(player, 0.0f);
	Player_SetForwardVelocity(player, 0.0f);
}

/// @brief Sets this #Player's rotational rate.
/// @param[out] player The player to modify.
/// @param[in] rate The new rate of rotation in radians per second.
void Player_SetRotationRate(Player * const player, const float rate) {
	player->rotationRate = rate;
}

/// @brief Sets this #Player's forward velocity.
/// @param[out] player The player to modify.
/// @param[in] velocity The new forward velocity in tile-lengths per second.
void Player_SetForwardVelocity(Player * const player, const float velocity) {
	player->forwardVelocity = velocity;
}

/// @brief Moves this #Player for a time interval based on movement and rotation rates.
/// @param[in,out] player The player state to update.
/// @param[in] seconds Time interval since the last update to simulate.
void Player_Update(Player * const player, const float seconds) {
	static const float pi = 3.14159265358979323846f;
	// Ideal distance assuming no collisions
	const float distanceIdeal = player->forwardVelocity * seconds;
	float distanceX, distanceY;
	player->rotation = fmodf(player->rotation + player->rotationRate * seconds, 2 * pi);
	distanceX = distanceIdeal * cosf(player->rotation);
	distanceY = distanceIdeal * sinf(player->rotation);

	// Determine X and Y collision separately, so the player can slide along a
	// wall they're touching. This also disallows phasing through the intersection
	// of two kitty-corner wall tiles.
	if (!World_IsTileTraversible(
		World_GetTileContents(player->world, player->x + distanceX, player->y))
	) { // Collision
		distanceX = 0.0f;
	}
	if (!World_IsTileTraversible(
		World_GetTileContents(player->world, player->x, player->y + distanceY))
	) { // Collision
		distanceY = 0.0f;
	}
	player->x += distanceX;
	player->y += distanceY;
	// Recalculate distance after collisions
	player->distanceTravelled += hypotf(distanceX, distanceY);
}

/// @return @c true if the #Player has navigated to an exit tile.
/// @param[in] player The player to query.
bool Player_HasWon(const Player * const player) {
	return World_GetTileContents(player->world, player->x, player->y) == TILE_EXIT;
}

/// @brief Renders this #Player's current view of the #World containing it.
/// @param[in] player The player whose viewpoint to draw.
/// @param[out] buffer The screen to overlay with world geometry.
void Player_DrawPerspective(const Player * const player, ScreenBuffer * const buffer) {
	World_DrawContents(player->world, buffer,
		player->x, player->y, player->rotation, playerHeight);
}
