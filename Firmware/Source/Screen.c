/// @file
/// @brief High-level screen timing and repainting functions.

#include "Screen.h"
#include "Nokia5110.h"
#include "ScreenBuffer.h"

/// @brief Initializes screen hardware for drawing.
void Screen_Initialize(void) {
	Nokia5110_Init();
}

/// @brief Draws a #ScreenBuffer's contents to the screen.
/// @note This call blocks while transmitting to the screen.
void Screen_DrawBuffer(const ScreenBuffer *buffer) {
	Nokia5110_DrawFullImage((const char *)buffer->buffer);
}
