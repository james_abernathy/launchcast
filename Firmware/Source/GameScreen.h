/// @file
/// @brief Actual gameplay screen for the player to interact with.

#ifndef _GAMESCREEN_H
#define _GAMESCREEN_H

#include "ScreenBuffer.h"
#include "World.h"

float GameScreen_Show(ScreenBuffer * const buffer, const WorldData * const worldData);

#endif // !defined(_GAMESCREEN_H)
