/// @file
/// @brief Sprite #Bitmap resources used for composing game screens.

#ifndef _SPRITES_H
#define _SPRITES_H

#include <stddef.h>
#include "Bitmap.h"

extern const Bitmap spriteButtonDown;
extern const Bitmap spriteButtonUp;
extern const Bitmap spriteEarth;
extern const Bitmap spriteExplosion;
extern const Bitmap spriteInvaderFrames[];
extern const size_t spriteInvaderFrameCount;
extern const Bitmap spriteLogo;
extern const Bitmap spriteLoseTextTimesUp;
extern const Bitmap spriteRocketDust;
extern const Bitmap spriteRocketHatchFrames[];
extern const size_t spriteRocketHatchFrameCount;
extern const Bitmap spriteRocketTrail;
extern const Bitmap spriteSlideKnob;
extern const Bitmap spriteSlideTrack;
extern const Bitmap spriteStar;
extern const Bitmap spriteWinTextLaunch;
extern const Bitmap spriteWinTextLift;
extern const Bitmap spriteWinTextOff;
extern const Bitmap spriteWinTextPad;

const Bitmap *Sprites_GetRocketByHeight(unsigned int height);

#endif // !defined(_SPRITES_H)
