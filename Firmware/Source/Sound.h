/// @file
/// @brief Sound generation driver.

#ifndef _SOUND_H
#define _SOUND_H

#include <stddef.h>

/// @brief Contains a 4-bit, 11.025kHz sound sample.
typedef struct SoundSample {
	size_t length; ///< The number of samples found within data.
	const unsigned char *data; ///< An array of 4-bit samples, packed two per byte.
} SoundSample;

void Sound_Initialize(void);
void Sound_Stop(void);
void Sound_PlayFrequency(const float frequency);
void Sound_PlaySample(const SoundSample * const sample, const unsigned int repetitions);

#endif // !defined(_SOUND_H)
