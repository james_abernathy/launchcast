/// @file
/// @brief #Bitmap type used for holding pre-rendered sprites.

#ifndef _BITMAP_H
#define _BITMAP_H

/// @brief Contains a pre-rendered 1-bit sprite that can be copied onto a #ScreenBuffer.
typedef struct Bitmap {
	/// @brief Width of this #Bitmap in pixels.
	/// @warning Must be greater than zero.
	unsigned char width;
	/// @brief Height of this #Bitmap in pixels.
	/// @warning Must be greater than zero.
	unsigned char height;
	/// @brief String of bytes containing 1-bit pixel contents.
	/// Bytes are stored in row-major order originating at the upper left,
	/// while contained bits represent a column of pixels.
	const unsigned char *buffer;
	/// @brief String of bytes containing 1-bit pixel mask contents, or @c NULL to not mask this #Bitmap.
	/// Values of @c 1 use the pixel in buffer, and @c 0 leaves the underlying screen pixel unchanged.
	/// Bytes are ordered the same as buffer.
	/// @warning This must be the same size as the buffer it masks.
	const unsigned char *maskBuffer;
} Bitmap;

#endif // !defined(_BITMAP_H)
