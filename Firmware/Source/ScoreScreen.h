/// @file
/// @brief Score and congratulations shown after a victory.

#ifndef _SCORESCREEN_H
#define _SCORESCREEN_H

#include "ScreenBuffer.h"

void ScoreScreen_Show(ScreenBuffer * const buffer, const float secondsRemaining);

#endif // !defined(_SCORESCREEN_H)
