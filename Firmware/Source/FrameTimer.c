/// @file
/// @brief Frame rate timing and other time-related routines.

#include "FrameTimer.h"
#include <math.h>
#include <stdbool.h>
#include "Startup.h"
#include "tm4c123gh6pm.h"

/// @brief Flag used by the SysTick interrupt to periodically signal the need for a screen repaint.
static volatile bool _refreshScreen = false;

/// @brief Initialize SysTick interrupts to trigger regularly.
void FrameTimer_Initialize(void) {
	static const unsigned char priority = 1; // Lower values are higher priority
	static const unsigned long clockFrequency = 80000000L; // 80 MHz

	unsigned long reloadPeriod;
	if (frameTimerFrequency <= 0) { // Invalid
		return;
	}
	reloadPeriod = clockFrequency / frameTimerFrequency + 0.5f; // Round
	if (reloadPeriod < 2 // Too fast for SysTick
		|| reloadPeriod > NVIC_ST_RELOAD_M - 1 // Too slow for SysTick
	) {
		return;
	}

	// Disable SysTick during setup
	NVIC_ST_CTRL_R &= ~NVIC_ST_CTRL_ENABLE;
	// Set priority
	NVIC_SYS_PRI3_R &= ~NVIC_PRI3_INT15_M;
	NVIC_SYS_PRI3_R |= priority << NVIC_PRI3_INT15_S;
	// Use internal clock, and fire interrupts
	NVIC_ST_CTRL_R |= NVIC_ST_CTRL_CLK_SRC | NVIC_ST_CTRL_INTEN;
	// Set frequency
	NVIC_ST_RELOAD_R = reloadPeriod - 1;
	NVIC_ST_CURRENT_R = 0; // Reset count

	_refreshScreen = false;
	// Begin firing SysTick_Handler
	NVIC_ST_CTRL_R |= NVIC_ST_CTRL_ENABLE;
}

/// @brief SysTick timer ISR to flag when a new frame should be painted to maintain its framerate.
void SysTick_Handler(void) {
	_refreshScreen = true;
}

/// @brief Halts execution until the screen should be painted again.
void FrameTimer_WaitUntilRefresh(void) {
	while (!_refreshScreen) {
		WaitForInterrupt(); // Wait for next SysTick pulse
	}
	_refreshScreen = false; // Reset for next call
}

/// @brief Delays the given number of frame refreshes.
/// @param[in] numFrames How many frame refresh intervals to pause for.
void FrameTimer_WaitFrames(const unsigned int numFrames) {
	unsigned int frame;
	for (frame = 0; frame < numFrames; ++frame) {
		FrameTimer_WaitUntilRefresh();
	}
}

/// @brief Formats a number of seconds into a minutes and seconds time string.
/// @param[in] seconds Positive number to convert to a string.
/// @param[out] buffer String buffer to add the formatted time to.
/// Must have at least frameTimerTextLength bytes of space plus one for a null terminator.
/// @return The address of the time string's null terminator within @p buffer.
char *FrameTimer_FormatTime(const float seconds, char *buffer) {
	static const unsigned char secondsPerMinute = 60;
	static const unsigned int secondsMax = 599; // 9:99
	static const unsigned int timeBase = 10;

	unsigned long digits = ceilf(seconds);
	if (digits > secondsMax) {
		digits = secondsMax;
	}

	// Minute
	*buffer++ = '0' + digits / secondsPerMinute;
	*buffer++ = ':';
	// Seconds
	digits %= secondsPerMinute;
	*buffer++ = '0' + digits / timeBase;
	*buffer++ = '0' + digits % timeBase;
	*buffer = '\0';
	return buffer;
}
