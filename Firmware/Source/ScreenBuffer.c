/// @file
/// @brief #ScreenBuffer type, along with drawing primitive routines for them.

#include "ScreenBuffer.h"
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include "Bitmap.h"
#include "Nokia5110.h" // Using font bitmap array

/// @brief Turns all pixels off for the given #ScreenBuffer.
/// @param[in,out] buffer Buffer to erase.
void ScreenBuffer_Clear(ScreenBuffer * const buffer) {
	memset(buffer->buffer, 0, screenBufferLength * sizeof(buffer->buffer[0]));
}

/// @brief Duplicates the contents of one #ScreenBuffer onto another.
/// @param[out] destination Buffer to overwrite with the contents of @p origin.
/// @param[in] origin Buffer to copy over @p destination.
void ScreenBuffer_Copy(ScreenBuffer * const destination, const ScreenBuffer * const origin) {
	if (destination == origin) {
		return;
	}
	memcpy(destination->buffer, origin->buffer,
		screenBufferLength * sizeof(origin->buffer[0]));
}

/// @return @c true if the given coordinate is within the screen.
/// @param[in] x,y Coordinate of pixel to test.
inline static bool _IsOnScreenBuffer(const int x, const int y) {
	return 0 <= x && x < (int)screenBufferWidth
		&& 0 <= y && y < (int)screenBufferHeight;
}

/// @return The byte index within a buffer containing the specified pixel coordinate.
/// @param[in] x,y Coordinate of pixel to index.
inline static unsigned int _IndexPixel(const unsigned char x, const unsigned char y) {
	return (unsigned int)(y / CHAR_BIT) * screenBufferWidth + x;
}

/// @brief Enables a pixel within this #ScreenBuffer.
/// @param[in,out] buffer The buffer to fill a pixel on.
/// @param[in] x,y Coordinate of pixel to fill.
void ScreenBuffer_PixelFill(ScreenBuffer * const buffer, const int x, const int y) {
	if (_IsOnScreenBuffer(x, y)) {
		buffer->buffer[_IndexPixel(x, y)] |= 1 << (y % CHAR_BIT);
	}
}

/// @brief Disables a pixel within this #ScreenBuffer.
/// @param[in,out] buffer The buffer to clear a pixel on.
/// @param[in] x,y Coordinate of pixel to clear.
void ScreenBuffer_PixelClear(ScreenBuffer * const buffer, const int x, const int y) {
	if (_IsOnScreenBuffer(x, y)) {
		buffer->buffer[_IndexPixel(x, y)] &= ~(1 << (y % CHAR_BIT));
	}
}

/// @return @c true if the given rectangle overlaps any part of the screen.
/// @param[in] left,top Upper-left corner coordinate of the rectangle.
/// @param[in] right,bottom Lower-right corner coordinate of the rectangle.
inline static bool _RectangleOverlapsScreen(
	const int left, const int top, const int right, const int bottom
) {
	return 0 <= right && left <= (int)screenBufferWidth - 1
		&& 0 <= bottom && top <= (int)screenBufferHeight - 1;
}

/// @brief Adjusts a value to lie within a valid range if necessary.
/// @param[in,out] value Number to clamp to a range.
/// @param[in] minimum Lowest allowable value of the range.
/// @param[in] maximum Highest allowable value of the range.
inline static void _Clamp(int * const value, const int minimum, const int maximum) {
	if (*value < minimum) {
		*value = minimum;
	} else if (*value > maximum) {
		*value = maximum;
	}
}

/// @brief Adjusts a coordinate to lie within the screen.
/// @param[in,out] x,y Coordinate to clamp if necessary.
inline static void _ClampToScreen(int * const x, int * const y) {
	_Clamp(x, 0, screenBufferWidth - 1);
	_Clamp(y, 0, screenBufferHeight - 1);
}

/// @brief Sets a section of the given row.
/// @param[out] buffer Buffer to draw the row to.
/// @param[in] row Row index to modify.
/// @param[in] left,right Inclusive range of columns to set within @p row.
/// @param[in] value The value to assign to pixels within the masked row.
/// @param[in] rowMask Mask to determine which pixel-rows to set within the byte-row.
inline static void _RowSet(ScreenBuffer * const buffer,
	const unsigned char row, const unsigned char left, const unsigned char right,
	const bool value, const unsigned char rowMask
) {
	// Pre-calculate offset of last byte
	unsigned char *rowOffset = &buffer->buffer[row * screenBufferWidth + right];
	int x;
	for (x = right - left; x >= 0; --x) {
		if (value) { // Set pixels
			*rowOffset-- |= rowMask;
		} else { // Clear pixels
			*rowOffset-- &= ~rowMask;
		}
	}
}

/// @return @c true if the sum of the two arguments would overflow.
/// @param[in] coordinate Signed coordinate value that will be summed with @p size.
/// @param[in] size Unsigned size value that will be summed with @p coordinate.
inline static bool _SumWillOverflow(const int coordinate, unsigned char size) {
	if (coordinate <= 0) {
		return false;
	}
	return INT_MAX - coordinate < (int)size;
}

/// @brief Sets all pixels within a rectangle to the given value.
/// @param[in,out] buffer Buffer to draw a rectangle on.
/// @param[in] left,top Corner to begin the rectangle at.
/// @param[in] width,height Size of the rectangle to draw.
/// @param[in] value @c true to fill the rectangle, or @c false to clear it.
void ScreenBuffer_RectangleSet(ScreenBuffer * const buffer,
	int left, int top, const unsigned char width, const unsigned char height, const bool value
) {
	static const unsigned char rowMask = ~0;
	unsigned char rowFirstMask = rowMask;
	unsigned char rowLastMask = rowMask;
	int row, rowFirst, rowLast;
	int right = left + (int)width - 1;
	int bottom = top + (int)height - 1;

	if (!width || !height
		|| _SumWillOverflow(left, width) || _SumWillOverflow(top, height)
		|| !_RectangleOverlapsScreen(left, top, right, bottom)
	) {
		return;
	}
	_ClampToScreen(&left, &top);
	_ClampToScreen(&right, &bottom);

	// Translate pixel Y-coordinates to byte rows
	rowFirst = top / CHAR_BIT;
	rowLast = bottom / CHAR_BIT;
	// First and last rows may not be entirely filled in
	rowFirstMask <<= top % CHAR_BIT;
	rowLastMask >>= CHAR_BIT - bottom % CHAR_BIT - 1;

	if (rowFirst == rowLast) { // No fully-filled rows
		_RowSet(buffer, rowFirst, left, right, value, rowFirstMask & rowLastMask);
		return;
	}
	_RowSet(buffer, rowFirst, left, right, value, rowFirstMask);
	// Fill solid middle rows
	for (row = rowFirst + 1; row < rowLast; ++row) {
		_RowSet(buffer, row, left, right, value, rowMask);
	}
	_RowSet(buffer, rowLast, left, right, value, rowLastMask);
}

/// @brief Writes the given string to this buffer without wrapping.
/// @param[out] buffer Buffer to write the text to.
/// @param[in] left,top Upper-left corner to begin the string at.
/// @param[in] text Null-terminated string to write. Characters without a defined glyph visual are ignored.
void ScreenBuffer_TextFill(ScreenBuffer * const buffer,
	int left, const int top, const char *text
) {
	static const unsigned char glyphBase = ' '; // First character within ASCII
	static const size_t numGlyphs = sizeof(ASCII) / sizeof(ASCII[0]);
	unsigned char current;
	// Point a fake Bitmap at character glyphs to print
	Bitmap glyph = {.width = screenBufferGlyphWidth, .height = screenBufferGlyphHeight};

	while ((current = *text++) != '\0') {
		if (glyphBase <= current && current < glyphBase + numGlyphs) { // Has a glyph
			glyph.buffer = (const unsigned char *)ASCII[current - glyphBase];
			ScreenBuffer_BitmapFill(buffer, left, top, &glyph);
			left += screenBufferGlyphWidth;
		}
	}
}

/// @brief Writes the given string horizontally centered within this buffer without wrapping.
/// @param[out] buffer Buffer to write the text to.
/// @param[in] top Top edge to begin the string at.
/// @param[in] text Null-terminated string to write. Characters without a defined glyph visual are ignored.
void ScreenBuffer_TextFillCentered(ScreenBuffer * const buffer,
	const int top, const char *text
) {
	ScreenBuffer_TextFill(buffer,
		((int)screenBufferWidth - strlen(text) * screenBufferGlyphWidth) / 2, top, text);
}

/// @brief BitBlit a masked bitmap onto a row of this screen.
/// @param[out] buffer Buffer to draw the row to.
/// @param[in] row Row index to modify. @p bitmap must overlap this row.
/// @param[in] left,right Inclusive range of columns to set within @p row.
/// @param[in] bitmapLeft The column within @p bitmap and @p mask to draw at @p left, in case of clipping.
/// @param[in] bitmapRow The row within @p bitmap and @p mask to sample from.
/// @param[in] bitmapShift How many pixels to shift @p bitmap and @p mask contents downward within @p row.
/// @param[in] bitmap Image to write onto @p buffer at the given position.
/// @param[in] rowMask Mask to determine which pixel-rows to set within the byte-row.
inline static void _RowBlit(ScreenBuffer * const buffer,
	const unsigned char row, const unsigned char left, const unsigned char right,
	const unsigned char bitmapLeft, const unsigned char bitmapRow, const signed char bitmapShift,
	const Bitmap * const bitmap, const unsigned char rowMask
) {
	// Pre-calculate offset of last bytes
	unsigned char *rowOffset = &buffer->buffer[row * screenBufferWidth + right];
	const unsigned char *bitmapOffset = &bitmap->buffer[bitmapRow * bitmap->width + bitmapLeft + (right - left)];
	const unsigned char *maskOffset = bitmap->maskBuffer
		? &bitmap->maskBuffer[bitmapRow * bitmap->width + bitmapLeft + (right - left)] : 0;
	unsigned char bitmapByte;
	unsigned char maskByte = ~0; // Unmasked, in case mask wasn't specified
	int x;
	for (x = right - left; x >= 0; --x) {
		// Get unmasked byte of pixels
		bitmapByte = *bitmapOffset--;
		if (bitmapShift > 0) {
			bitmapByte <<= bitmapShift; // Shift pixels down
		} else {
			bitmapByte >>= -bitmapShift; // Shift pixels up
		}
		if (maskOffset) {
			// Get mask byte of pixels
			maskByte = *maskOffset--;
			if (bitmapShift > 0) {
				maskByte <<= bitmapShift; // Shift pixels down
			} else {
				maskByte >>= -bitmapShift; // Shift pixels up
			}
		}
		*rowOffset ^= (*rowOffset ^ bitmapByte) & (rowMask & maskByte);
		--rowOffset;
	}
}

/// @return The number of byte rows within a #Bitmap image.
/// @param[in] bitmap The image to measure.
inline static unsigned char _GetNumBitmapRows(const Bitmap * const bitmap) {
	return (bitmap->height + CHAR_BIT - 1) / CHAR_BIT; // Round up
}

/// @brief Copies a #Bitmap onto this buffer.
/// @param[out] buffer Buffer to copy the image to.
/// @param[in] left,top Upper-left corner to begin copying to.
/// @param[in] bitmap Image to write onto @p buffer at the given position.
void ScreenBuffer_BitmapFill(ScreenBuffer * const buffer,
	int left, int top, const Bitmap * const bitmap
) {
	static const unsigned char rowMask = ~0;
	int right = left + (int)bitmap->width - 1;
	int bottom = top + (int)bitmap->height - 1;
	// Top-left corner on bitmap to start copying from
	int bitmapLeft = -left;
	int bitmapTop = -top;
	unsigned char bitmapRow, bitmapShiftMask;
	signed char bitmapShift;
	unsigned char rowFirstMask = rowMask;
	unsigned char rowLastMask = rowMask;
	int row, rowFirst, rowLast;

	if (_SumWillOverflow(left, bitmap->width) || _SumWillOverflow(top, bitmap->height)
		|| !_RectangleOverlapsScreen(left, top, right, bottom)
	) {
		return;
	}
	bitmapShift = top % CHAR_BIT;
	if (bitmapShift < 0) { // Got remainder rather than modulus
		bitmapShift += CHAR_BIT;
	}
	bitmapShiftMask = ~0 << bitmapShift;
	_ClampToScreen(&left, &top);
	_ClampToScreen(&right, &bottom);
	bitmapLeft += left;
	bitmapTop += top;
	bitmapRow = (bitmapTop + CHAR_BIT - 1) / CHAR_BIT; // Round up

	// Translate pixel Y-coordinates to byte rows
	rowFirst = top / CHAR_BIT;
	rowLast = bottom / CHAR_BIT;
	// First and last rows may not be entirely filled in
	rowFirstMask <<= top % CHAR_BIT;
	rowLastMask >>= CHAR_BIT - bottom % CHAR_BIT - 1;
	if (rowFirst == rowLast) { // Only one row
		rowFirstMask &= rowLastMask;
	}

	// First row
	bitmapShiftMask = ~0 << bitmapShift;
	if (bitmapShift && bitmapRow > 0) {
		// Draw the part of off-screen row that was shifted down
		_RowBlit(buffer, rowFirst, left, right, bitmapLeft, bitmapRow - 1, bitmapShift - CHAR_BIT,
			bitmap, rowFirstMask & ~bitmapShiftMask);
	}
	if (rowFirst == rowLast && bitmapRow >= _GetNumBitmapRows(bitmap)) {
		return; // Only row was partially above the screen
	}
	_RowBlit(buffer, rowFirst, left, right, bitmapLeft, bitmapRow++, bitmapShift,
		bitmap, rowFirstMask & bitmapShiftMask);
	if (rowFirst == rowLast) {
		return;
	}

	// Middle rows
	for (row = rowFirst + 1; row < rowLast; ++row) {
		if (bitmapShift) {
			// Draw the part of last row that was shifted down
			_RowBlit(buffer, row, left, right, bitmapLeft, bitmapRow - 1, bitmapShift - CHAR_BIT,
				bitmap, rowMask & ~bitmapShiftMask);
		}
		_RowBlit(buffer, row, left, right, bitmapLeft, bitmapRow++, bitmapShift,
			bitmap, rowMask & bitmapShiftMask);
	}
	// Last row
	if (bitmapShift) {
		// Draw the part of last row that was shifted down
		_RowBlit(buffer, rowLast, left, right, bitmapLeft, bitmapRow - 1, bitmapShift - CHAR_BIT,
			bitmap, rowLastMask & ~bitmapShiftMask);
	}
	_RowBlit(buffer, rowLast, left, right, bitmapLeft, bitmapRow, bitmapShift,
		bitmap, rowLastMask & bitmapShiftMask);
}

/// @brief Copies a single column from a #Bitmap onto this buffer.
/// @param[out] buffer Buffer to copy the column to.
/// @param[in] left,top Upper-left corner to start copying the column.
/// @param[in] bitmapColumn Column to copy from @p bitmap.
/// @param[in] bitmap Image to copy a single column from.
void ScreenBuffer_BitmapFillColumn(ScreenBuffer * const buffer,
	int left, int top, const int bitmapColumn, const Bitmap * const bitmap
) {
	static const unsigned char rowMask = ~0;
	int bottom = top + (int)bitmap->height - 1;
	// Top edge on bitmap to start copying from
	int bitmapTop = -top;
	unsigned char bitmapRow, bitmapShiftMask;
	signed char bitmapShift;
	unsigned char rowFirstMask = rowMask;
	unsigned char rowLastMask = rowMask;
	int row, rowFirst, rowLast;

	if (bitmapColumn < 0 || bitmap->width <= bitmapColumn
		|| _SumWillOverflow(top, bitmap->height)
		|| !_RectangleOverlapsScreen(left, top, left, bottom)
	) {
		return;
	}
	bitmapShift = top % CHAR_BIT;
	if (bitmapShift < 0) { // Got remainder rather than modulus
		bitmapShift += CHAR_BIT;
	}
	bitmapShiftMask = ~0 << bitmapShift;
	_ClampToScreen(&left, &top);
	_ClampToScreen(&left, &bottom);
	bitmapTop += top;
	bitmapRow = (bitmapTop + CHAR_BIT - 1) / CHAR_BIT; // Round up

	// Translate pixel Y-coordinates to byte rows
	rowFirst = top / CHAR_BIT;
	rowLast = bottom / CHAR_BIT;
	// First and last rows may not be entirely filled in
	rowFirstMask <<= top % CHAR_BIT;
	rowLastMask >>= CHAR_BIT - bottom % CHAR_BIT - 1;
	if (rowFirst == rowLast) { // Only one row
		rowFirstMask &= rowLastMask;
	}

	// First row
	bitmapShiftMask = ~0 << bitmapShift;
	if (bitmapShift && bitmapRow > 0) {
		// Draw the part of off-screen row that was shifted down
		_RowBlit(buffer, rowFirst, left, left, bitmapColumn, bitmapRow - 1, bitmapShift - CHAR_BIT,
			bitmap, rowFirstMask & ~bitmapShiftMask);
	}
	if (rowFirst == rowLast && bitmapRow >= _GetNumBitmapRows(bitmap)) {
		return; // Only row was partially above the screen
	}
	_RowBlit(buffer, rowFirst, left, left, bitmapColumn, bitmapRow++, bitmapShift,
		bitmap, rowFirstMask & bitmapShiftMask);
	if (rowFirst == rowLast) {
		return;
	}

	// Middle rows
	for (row = rowFirst + 1; row < rowLast; ++row) {
		if (bitmapShift) {
			// Draw the part of last row that was shifted down
			_RowBlit(buffer, row, left, left, bitmapColumn, bitmapRow - 1, bitmapShift - CHAR_BIT,
				bitmap, rowMask & ~bitmapShiftMask);
		}
		_RowBlit(buffer, row, left, left, bitmapColumn, bitmapRow++, bitmapShift,
			bitmap, rowMask & bitmapShiftMask);
	}
	// Last row
	if (bitmapShift) {
		// Draw the part of last row that was shifted down
		_RowBlit(buffer, rowLast, left, left, bitmapColumn, bitmapRow - 1, bitmapShift - CHAR_BIT,
			bitmap, rowLastMask & ~bitmapShiftMask);
	}
	_RowBlit(buffer, rowLast, left, left, bitmapColumn, bitmapRow, bitmapShift,
		bitmap, rowLastMask & bitmapShiftMask);
}

/// @brief Copies a #Bitmap onto the center of this buffer.
/// @param[out] buffer Buffer to copy the image to.
/// @param[in] bitmap Image to write onto the center of @p buffer.
void ScreenBuffer_BitmapFillCentered(ScreenBuffer * const buffer, const Bitmap * const bitmap) {
	ScreenBuffer_BitmapFill(buffer,
		((int)screenBufferWidth - (int)bitmap->width) / 2,
		((int)screenBufferHeight - (int)bitmap->height) / 2, bitmap);
}
