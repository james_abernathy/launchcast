/// @file
/// @brief #World game object and routines for manipulating it.

#ifndef _WORLD_H
#define _WORLD_H

#include <stdbool.h>
#include "ScreenBuffer.h"

/// @brief Possible contents of tiles within a #World.
typedef enum WorldTile {
	TILE_INVALID = 0, ///< The loaded #WorldData contained an unrecognized tile character.
	TILE_SPACE, ///< The tile is unoccuppied.
	TILE_WALL, ///< The tile contains a solid wall that cannot be passed.
	TILE_START, ///< The tile is a #Player spawn point, but otherwise acts like a TILE_SPACE.
	TILE_ROCKET, ///< The tile has a rocket sprite on it, and acts like a TILE_WALL.
	TILE_EXIT ///< The tile acts like a TILE_SPACE, but passing through it lets the #Player win.
} WorldTile;

/// @brief Unloaded #World geometry.
typedef struct WorldData {
	unsigned int sizeX; ///< Tile count along the X axis.
	unsigned int sizeY; ///< Tile count along the Y axis.
	float timeLimit; ///< Positive max number of seconds a player has to finish this world.
	/// @brief Tile contents expressed as a string constant.
	/// Each character represents the contents of a tile on the map, stored in
	/// row-major order from the top-left.
	///
	/// Here is a list of tile characters with their meanings:
	///   - @c ' ' = Empty space.
	///   - @c '#' = Wall.
	///   - @c 'S' = Player start position. Exactly one must be present per world.
	///   - @c 'R' = Rocket sprite position. Exactly one must be present per world.
	///   - @c '@' = Transparent exit tiles that will end the level with a win.
	const char *contents;
} WorldData;

/// @brief Active level that a #Player can traverse.
typedef struct World {
	const WorldData *data; ///< Loaded level geometry.
	int startX; ///< Starting tile X-coordinate that the player will be centered within.
	int startY; ///< Starting tile Y-coordinate that the player will be centered within.
	int rocketX; ///< Tile X-coordinate that the rocket sprite should be centered on.
	int rocketY; ///< Tile Y-coordinate that the rocket sprite should be centered on.
} World;

bool World_LoadData(World * const world, const WorldData * const data);
void World_DrawBackground(const World * const world, ScreenBuffer * const buffer);
void World_DrawContents(const World * const world, ScreenBuffer * const buffer, const float eyeX, const float eyeY, const float eyeAngle, const float eyeHeight);
float World_GetTimeLimit(const World * const world);
void World_GetStart(const World * const world, float * const x, float * const y);
void World_GetRocket(const World * const world, float * const x, float * const y);
WorldTile World_GetTileContents(const World * const world, const float x, const float y);
bool World_IsTileTraversible(const WorldTile tile);

#endif // !defined(_WORLD_H)
