/// @file
/// @brief Animation shown after the player loses.

#include "LoseScreen.h"
#include <stdbool.h>
#include <stddef.h>
#include "Bitmap.h"
#include "Controls.h"
#include "FrameTimer.h"
#include "Screen.h"
#include "ScreenBuffer.h"
#include "Sound.h"
#include "SoundSamples.h"
#include "Sprites.h"

/// @brief The movement and animation properties of a single space invader.
typedef struct Invader {
	unsigned int framePeriod; ///< The number of frames between sprite frame changes.
	int startLeft; ///< Starting left edge of this sprite.
	int startTop; ///< Starting top edge of this sprite.
	int endLeft; ///< Ending left edge of this sprite.
	int endTop; ///< Ending top edge of this sprite.
} Invader;

/// @return This #Invader's animation #Bitmap to display.
/// @param[in] invader Invader profile to find the animation frame of.
/// @param[in] frame Screen update number to get an animation frame for.
static inline const Bitmap *_InvaderGetBitmap(Invader const * const invader, const unsigned int frame) {
	return &spriteInvaderFrames[(frame / invader->framePeriod) % spriteInvaderFrameCount];
}

/// @return The interpolated position between two end-points over time.
/// @param[in] start Beginning position, when @p frame is @c 0.
/// @param[in] end Ending position, when @p frame is equal to @p frameMax.
/// @param[in] frame Screen update number to get an animation frame for.
/// @param[in] frameMax The screen update at which the movement should complete.
static inline int _Interpolate(const int start, const int end,
	const unsigned int frame, const unsigned int frameMax
) {
	return start + (int)frame * (end - start) / (int)frameMax;
}

/// @brief Shows an un-skippable game over animation.
/// @param[out] buffer Shared #ScreenBuffer to use for rendering.
void LoseScreen_Show(ScreenBuffer * const buffer) {
	// How many frame updates the invaders take to reach earth.
	static const unsigned int invasionFrames = 5.0f * frameTimerFrequency;
	// How many frame updates to wait between invader sounds during invasionFrames.
	static const unsigned int invaderSoundFrames = 0.5f * frameTimerFrequency;
	// How many frame updates Earth's explosion remains on screen.
	static const unsigned int explosionFrames = 2.0f * frameTimerFrequency;

	static const Invader invaders[] = {
		{.framePeriod = 29, .startLeft = -26, .startTop =  42, .endLeft = 12, .endTop = 22}, // From bottom-left
		{.framePeriod = 18, .startLeft =  17, .startTop =  -8, .endLeft = 27, .endTop =  0}, // From top
		{.framePeriod = 24, .startLeft =  84, .startTop = -11, .endLeft = 53, .endTop =  3}, // From top-right
		{.framePeriod = 15, .startLeft =  89, .startTop =  32, .endLeft = 58, .endTop = 26}, // From right
		{.framePeriod = 21, .startLeft =  39, .startTop =  58, .endLeft = 36, .endTop = 38}  // From bottom
	};
	static const size_t invaderCount = sizeof(invaders) / sizeof(invaders[0]);

	unsigned int frameCount;
	size_t index;
	const Bitmap *bitmap;
	const Invader *invader;

	ScreenBuffer_Clear(buffer);
	// Add some stars
	ScreenBuffer_BitmapFill(buffer, 70, 10, &spriteStar);
	ScreenBuffer_BitmapFill(buffer, 20, 40, &spriteStar);
	ScreenBuffer_PixelFill(buffer, 8, 21);
	ScreenBuffer_PixelFill(buffer, 17, 7);
	ScreenBuffer_PixelFill(buffer, 62, 42);
	// Center the earth
	ScreenBuffer_BitmapFillCentered(buffer, &spriteEarth);

	// Let invaders slowly fly in
	for (frameCount = 0; frameCount < invasionFrames; ++frameCount) {
		if (frameCount % invaderSoundFrames == 0) {
			Sound_PlaySample(&soundInvader, 1);
		}
		for (index = 0; index < invaderCount; ++index) {
			invader = &invaders[index];
			ScreenBuffer_BitmapFill(buffer,
				_Interpolate(invader->startLeft, invader->endLeft, frameCount, invasionFrames),
				_Interpolate(invader->startTop, invader->endTop, frameCount, invasionFrames),
				_InvaderGetBitmap(invader, frameCount));
		}
		Screen_DrawBuffer(buffer);
		FrameTimer_WaitUntilRefresh();
		// Clear old positions for next frame
		for (index = 0; index < invaderCount; ++index) {
			invader = &invaders[index];
			bitmap = _InvaderGetBitmap(invader, frameCount);
			ScreenBuffer_RectangleSet(buffer,
				_Interpolate(invader->startLeft, invader->endLeft, frameCount, invasionFrames),
				_Interpolate(invader->startTop, invader->endTop, frameCount, invasionFrames),
				bitmap->width, bitmap->height, false);
		}
	}

	// Blow it up!
	Sound_PlaySample(&soundExplosionBig, 1);
	ScreenBuffer_RectangleSet(buffer,
		((int)screenBufferWidth - (int)spriteEarth.width) / 2,
		((int)screenBufferHeight - (int)spriteEarth.height) / 2,
		spriteEarth.width, spriteEarth.height, false);
	ScreenBuffer_BitmapFillCentered(buffer, &spriteExplosion);
	for (frameCount = invasionFrames; frameCount < invasionFrames + explosionFrames; ++frameCount) {
		// Continue animating invaders
		for (index = 0; index < invaderCount; ++index) {
			invader = &invaders[index];
			ScreenBuffer_BitmapFill(buffer, invader->endLeft, invader->endTop,
				_InvaderGetBitmap(invader, frameCount));
		}
		Screen_DrawBuffer(buffer);
		FrameTimer_WaitUntilRefresh();
	}

	// Show game over text
	ScreenBuffer_Clear(buffer);
	ScreenBuffer_BitmapFillCentered(buffer, &spriteLoseTextTimesUp);
	Screen_DrawBuffer(buffer);
	Controls_WaitForButtonPress();
}
