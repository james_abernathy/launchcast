/// @file
/// @brief Function headers for Startup.c's assembly routines.

#ifndef _STARTUP_H
#define _STARTUP_H

void DisableInterrupts(void);
void EnableInterrupts(void);
void WaitForInterrupt(void);

void StartCritical(void);
void EndCritical(void);

#endif // !defined(_STARTUP_H)
