/// @file
/// @brief #Player game object and routines for manipulating it.

#ifndef _PLAYER_H
#define _PLAYER_H

#include <stdbool.h>
#include "ScreenBuffer.h"
#include "World.h"

/// @brief State of a player that can navigate #World instances.
typedef struct Player {
	const World *world; ///< The #World that this #Player is navigating.
	float x; ///< X-coordinate within the world, measured in tiles.
	float y; ///< Y-coordinate within the world, measured in tiles.
	float forwardVelocity; ///< Forward velocity in tiles per second.
	float rotation; ///< Angle that this #Player is facing, in radians.
	float rotationRate; ///< Rotational rate of change, in radians per second.
	float distanceTravelled; ///< Total distance travelled in tile lengths.
} Player;

void Player_EnterWorld(Player * const player, const World * const world);
void Player_SetRotationRate(Player * const player, const float rate);
void Player_SetForwardVelocity(Player * const player, const float velocity);
void Player_Update(Player * const player, const float seconds);
bool Player_HasWon(const Player * const player);
void Player_DrawPerspective(const Player * const player, ScreenBuffer * const buffer);

#endif // !defined(_PLAYER_H)
