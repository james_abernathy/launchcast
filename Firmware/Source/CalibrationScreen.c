/// @file
/// @brief Initial calibration screen to familiarize players with controls and center the slider.

#include "CalibrationScreen.h"
#include <math.h>
#include <stdbool.h>
#include "Controls.h"
#include "FrameTimer.h"
#include "Random.h"
#include "Screen.h"
#include "ScreenBuffer.h"
#include "Sound.h"
#include "SoundSamples.h"
#include "Sprites.h"

/// @brief Animates the screen while waiting for the user to press up.
/// @param[out] buffer Shared #ScreenBuffer to use for rendering.
/// @return Number of frames the user took to finish calibration.
static unsigned long _CalibrationScreenAnimate(ScreenBuffer * const buffer) {
	// Total radians for the slide's cosine to travel through.
	static const float slideThetaMax = 12;
	// How many frame updates the slider takes to settle at its center.
	static const unsigned int slideSettlingFrames = 3.0f * frameTimerFrequency;
	// How many frame updates the button press animation will take.
	static const unsigned int buttonPressFrames = 1.0f * frameTimerFrequency;

	// Top-left coordinate to draw the slider at.
	static const int slideLeft = 2;
	static const int slideTop = 41;
	// How close the slider knob will come to the track edges while animating.
	static const int slidePadding = 5;
	// Y-coordinate to draw the slider knob's top edge at.
	static const int knobTop = 36;
	// X-coordinate to draw both buttons' left edges at.
	static const int buttonsLeft = 66;
	// Y-coordinate to draw each buttons' top edge at.
	static const int buttonUpTop = 25;
	static const int buttonDownTop = 37;

	static const unsigned int lineHeight = screenBufferGlyphHeight;
	const int knobOffset = slideLeft + spriteSlideTrack.width / 2 - spriteSlideKnob.width / 2;
	const unsigned int knobAmplitude = (spriteSlideTrack.width - slidePadding * 2 - spriteSlideKnob.width) / 2;

	ButtonState upState;
	unsigned long frameTotal = 0;
	unsigned int frameCount;
	float progress;

	ScreenBuffer_Clear(buffer);
	// Static sprites
	ScreenBuffer_TextFillCentered(buffer, 0 * lineHeight, "Center your");
	ScreenBuffer_TextFillCentered(buffer, 1 * lineHeight, "steering slide,");
	ScreenBuffer_TextFillCentered(buffer, 2 * lineHeight, "then press up.");
	ScreenBuffer_BitmapFill(buffer, buttonsLeft, buttonUpTop, &spriteButtonUp); // Up button
	ScreenBuffer_BitmapFill(buffer, buttonsLeft, buttonDownTop, &spriteButtonUp); // Down button

	ButtonState_Initialize(&upState, Controls_IsUpPressed());
	while (true) {
		// Slide the knob to the center
		for (frameCount = 0; frameCount < slideSettlingFrames; ++frameCount) {
			++frameTotal;
			progress = (float)frameCount / slideSettlingFrames;
			// Erase knob's last position
			ScreenBuffer_RectangleSet(buffer, slideLeft, knobTop,
				spriteSlideTrack.width, slideTop - knobTop, false);
			// Re-draw slider
			ScreenBuffer_BitmapFill(buffer, slideLeft, slideTop, &spriteSlideTrack);
			ScreenBuffer_BitmapFill(buffer,
				// Oscillate around center of track, damping the amplitude over time
				knobOffset + knobAmplitude * cosf(slideThetaMax * progress) * (1.0f - progress) + 0.5f,
				knobTop, &spriteSlideKnob);
			Screen_DrawBuffer(buffer);
			FrameTimer_WaitUntilRefresh();
			if (ButtonState_PressCompleted(&upState, Controls_IsUpPressed())) {
				return frameTotal;
			}
		}

		// Press the up button graphic
		ScreenBuffer_BitmapFill(buffer, buttonsLeft, buttonUpTop, &spriteButtonDown);
		Screen_DrawBuffer(buffer);
		for (frameCount = 0; frameCount < buttonPressFrames / 2; ++frameCount) {
			++frameTotal;
			FrameTimer_WaitUntilRefresh();
			if (ButtonState_PressCompleted(&upState, Controls_IsUpPressed())) {
				return frameTotal;
			}
		}
		// Release the up button graphic
		ScreenBuffer_BitmapFill(buffer, buttonsLeft, buttonUpTop, &spriteButtonUp);
		Screen_DrawBuffer(buffer);
		for (frameCount = buttonPressFrames / 2; frameCount < buttonPressFrames; ++frameCount) {
			++frameTotal;
			FrameTimer_WaitUntilRefresh();
			if (ButtonState_PressCompleted(&upState, Controls_IsUpPressed())) {
				return frameTotal;
			}
		}
	}
}

/// @brief Prompts the player to center their steering slider, and seeds the RNG.
void CalibrationScreen_Show(ScreenBuffer * const buffer) {
	const unsigned long framesTaken = _CalibrationScreenAnimate(buffer);
	// Seed with time taken and slider noise/movement
	Random_Init(framesTaken + (unsigned long)Controls_GetSliderOffset());
	Controls_ZeroSlider();
	Sound_PlaySample(&soundShoot, 1);
}
