/// @file
/// @brief Brief introductory screen describing the player's goal.

#ifndef _INSTRUCTIONSSCREEN_H
#define _INSTRUCTIONSSCREEN_H

#include "ScreenBuffer.h"

void InstructionsScreen_Show(ScreenBuffer * const buffer);

#endif // !defined(_INSTRUCTIONSSCREEN_H)
