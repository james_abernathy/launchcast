/// @file
/// @brief Brief introductory screen describing the player's goal.

#include "InstructionsScreen.h"
#include "Controls.h"
#include "Screen.h"
#include "ScreenBuffer.h"
#include "Sound.h"
#include "SoundSamples.h"

/// @brief Shows a brief explanation of the time limit and goal.
/// @param[out] buffer Shared #ScreenBuffer to use for rendering.
void InstructionsScreen_Show(ScreenBuffer * const buffer) {
	static const unsigned int lineHeight = screenBufferGlyphHeight;

	ScreenBuffer_Clear(buffer);
	ScreenBuffer_TextFillCentered(buffer, 0 * lineHeight, "We're being");
	ScreenBuffer_TextFillCentered(buffer, 1 * lineHeight, "invaded!");

	ScreenBuffer_TextFillCentered(buffer, 3 * lineHeight, "Find your ship");
	ScreenBuffer_TextFillCentered(buffer, 4 * lineHeight, "and escape");
	ScreenBuffer_TextFillCentered(buffer, 5 * lineHeight, "to space!");

	Screen_DrawBuffer(buffer);
	Controls_WaitForButtonPress();
	Sound_PlaySample(&soundShoot, 1);
}
