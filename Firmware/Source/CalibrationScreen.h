/// @file
/// @brief Initial calibration screen to familiarize players with controls and center the slider.

#ifndef _CALIBRATIONSCREEN_H
#define _CALIBRATIONSCREEN_H

#include "ScreenBuffer.h"

void CalibrationScreen_Show(ScreenBuffer * const buffer);

#endif // !defined(_CALIBRATIONSCREEN_H)
