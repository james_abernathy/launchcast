/// @file
/// @brief Score and congratulations shown after a victory.

#include "ScoreScreen.h"
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include "Controls.h"
#include "FrameTimer.h"
#include "Screen.h"
#include "ScreenBuffer.h"
#include "Sound.h"
#include "SoundSamples.h"
#include "Sprites.h"

/// @brief Position and animation info for a single star sprite.
typedef struct Star {
	unsigned int blinkFramePeriod; ///< Frames between star blinks.
	int x; ///< Center X-coordinate of this #Star.
	int y; ///< Center Y-coordinate of this #Star.
} Star;

/// @brief Position and timing info about a single dying invader sprite.
typedef struct DyingInvader {
	const Bitmap *bitmap; ///< The static #Bitmap used before the invader's death.
	unsigned int deathFrame; ///< The frame time when this #DyingInvader explodes.
	int x; ///< Center X-coordinate of this #DyingInvader.
	int y; ///< Center Y-coordinate of this #DyingInvader.
} DyingInvader;

/// @brief Displays the player's score and a simple repeating animation.
/// @param[out] buffer Shared #ScreenBuffer to use for rendering.
/// @param[in] secondsRemaining Number of extra seconds the player had after completing the game.
void ScoreScreen_Show(ScreenBuffer * const buffer, const float secondsRemaining) {
	// How many frame updates the flight arc should take to draw.
	static const unsigned int flightFrames = 5.0f * frameTimerFrequency;
	// How many frames invader explosions should be visible for.
	static const unsigned int invaderDeathFrames = 1.0f * frameTimerFrequency;

	static const char timeTextSuffix[] = " to spare!";
	static const size_t timeTextMaxLength = frameTimerTextLength + sizeof(timeTextSuffix);
	// Angle in radians that the rocket should leave Earth at.
	static const float spiralAngleOffset = 0.65f;
	// Max angle in radians for the spiral to draw out.
	static const float spiralAngleMax = 4.4f;
	// Arbitrary positive constants of the logarithmic spiral formula.
	static const float spiralA = 0.5f;
	static const float spiralB = 1.1f;
	// The center coordinate of the spiral launch path.
	static const int spiralCenterX = (int)screenBufferWidth / 2 + 13;
	static const int spiralCenterY = (int)screenBufferHeight - 10;

	static const Star stars[] = {
		{.blinkFramePeriod = 24, .x =  4, .y = 38},
		{.blinkFramePeriod = 16, .x = 67, .y = 44},
		{.blinkFramePeriod = 21, .x = 80, .y =  6},
		{.blinkFramePeriod = 32, .x =  1, .y = 10},
	};
	static const size_t starCount = sizeof(stars) / sizeof(stars[0]);
	static const DyingInvader dyingInvaders[] = {
		{.bitmap = &spriteInvaderFrames[0], .deathFrame =  40, .x = 63, .y = 33},
		{.bitmap = &spriteInvaderFrames[1], .deathFrame = 130, .x = 18, .y = 31}
	};
	static const size_t dyingInvaderCount = sizeof(dyingInvaders) / sizeof(dyingInvaders[0]);

	ButtonState upState, downState;
	unsigned int frameCount = 0;
	size_t index;
	const unsigned char lineHeight = screenBufferGlyphHeight;
	char timeText[timeTextMaxLength];
	float spiralAngle, spiralRadius;
	const DyingInvader *invader;

	// Static sprites
	ScreenBuffer_Clear(buffer);
	ScreenBuffer_TextFillCentered(buffer, lineHeight * 0, "You saved");
	ScreenBuffer_TextFillCentered(buffer, lineHeight * 1, "Earth with");
	strncpy(FrameTimer_FormatTime(secondsRemaining, timeText), timeTextSuffix, sizeof(timeTextSuffix));
	ScreenBuffer_TextFillCentered(buffer, lineHeight * 2, timeText);
	ScreenBuffer_BitmapFill(buffer,
		((int)screenBufferWidth - (int)spriteEarth.width) / 2,
		(int)screenBufferHeight - (int)spriteEarth.height / 2, &spriteEarth);
	// Invaders in the rocket's path
	for (index = 0; index < dyingInvaderCount; ++index) {
		invader = &dyingInvaders[index];
		ScreenBuffer_BitmapFill(buffer,
			invader->x - (int)invader->bitmap->width / 2,
			invader->y - (int)invader->bitmap->height / 2, invader->bitmap);
	}

	ButtonState_Initialize(&upState, Controls_IsUpPressed());
	ButtonState_Initialize(&downState, Controls_IsDownPressed());
	while (true) {
		frameCount++;
		// Add blinking stars
		for (index = 0; index < starCount; ++index) {
			if ((frameCount / stars[index].blinkFramePeriod) % 2) {
				ScreenBuffer_PixelFill(buffer, stars[index].x, stars[index].y);
			} else {
				ScreenBuffer_BitmapFill(buffer,
					stars[index].x - (int)spriteStar.width / 2,
					stars[index].y - (int)spriteStar.height / 2, &spriteStar);
			}
		}
		// Gradually draw launch spiral
		if (frameCount < flightFrames) {
			spiralAngle = spiralAngleMax * frameCount / flightFrames;
			// Logarithmic spiral
			spiralRadius = spiralA * expf(spiralB * spiralAngle);
			ScreenBuffer_PixelFill(buffer,
				spiralCenterX + spiralRadius * cosf(spiralAngleOffset - spiralAngle),
				spiralCenterY + spiralRadius * sinf(spiralAngleOffset - spiralAngle));
		}
		// Blow up invaders as we pass
		for (index = 0; index < dyingInvaderCount; ++index) {
			invader = &dyingInvaders[index];
			if (frameCount == invader->deathFrame) {
				Sound_PlaySample(&soundExplosion, 1);
				// Replace with explosion
				ScreenBuffer_RectangleSet(buffer,
					invader->x - (int)invader->bitmap->width / 2,
					invader->y - (int)invader->bitmap->height / 2,
					invader->bitmap->width, invader->bitmap->height, false);
				ScreenBuffer_BitmapFill(buffer,
					invader->x - (int)spriteExplosion.width / 2,
					invader->y - (int)spriteExplosion.height / 2, &spriteExplosion);
			} else if (frameCount == invader->deathFrame + invaderDeathFrames) {
				// Remove explosion debris
				ScreenBuffer_RectangleSet(buffer,
					invader->x - (int)spriteExplosion.width / 2,
					invader->y - (int)spriteExplosion.height / 2,
					spriteExplosion.width, spriteExplosion.height, false);
			}
		}

		Screen_DrawBuffer(buffer);
		FrameTimer_WaitUntilRefresh();
		if (ButtonState_PressCompleted(&upState, Controls_IsUpPressed())
			|| ButtonState_PressCompleted(&downState, Controls_IsDownPressed())
		) {
			return;
		}

		// Erase blinking stars
		for (index = 0; index < starCount; ++index) {
			ScreenBuffer_RectangleSet(buffer,
				stars[index].x - (int)spriteStar.width / 2,
				stars[index].y - (int)spriteStar.height / 2,
				spriteStar.width, spriteStar.height, false);
		}
	}
}
