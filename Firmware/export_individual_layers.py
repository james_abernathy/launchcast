""" GIMP 2.8.1+ Python plugin to split a layered image (such as *.XCF) into separate images.
Layer masks are inverted before extraction.
"""

from gimpfu import *
import os.path

def export_individual_layers(image, __unused_drawable, directory, name_pattern):
  def save_drawable(drawable, name):
    filename = name_pattern % name
    file_path = os.path.join(directory, filename)
    pdb.gimp_file_save(image, drawable, file_path, filename)

  def recurse_layers(layers):
    for layer in layers:
      if pdb.gimp_item_is_group(layer):
        recurse_layers(layer.layers)
      else:
        save_drawable(layer, layer.name)
        if layer.mask:
          pdb.gimp_invert(layer.mask)
          save_drawable(layer.mask, layer.mask.name)

  try:
    # Work in a copy of the image, to avoid polluting the undo history
    duplicate = pdb.gimp_image_duplicate(image)
    duplicate.disable_undo()
    recurse_layers(duplicate.layers)
  finally:
    if duplicate:
      gimp.delete(duplicate)

register(
  'python_fu_export_individual_layers',
  'Save all layers into separate files, including those within groups.\n\nLayer masks are also saved after being inverted. Note that all layer names must be valid filenames!',
  '',
  'James Abernathy',
  '',
  '2015-04-29',
  '<Image>/File/Export individual layers...',
  '*',
  [
    (PF_DIRNAME, 'directory', 'Directory to save layer images within.', '/'),
    (PF_STRING, 'name_pattern', 'Pattern for file names with any image format extension, where "%s" will be replaced with the layer\'s name.', '%s.bmp')],
  [],
  export_individual_layers)

main()